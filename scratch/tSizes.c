#include <stdio.h>
#include <stdlib.h>

int main(void)
{
/*
	char*   cp;
	double* dp;
	float*  fp;
	int*    ip;
*/	
	void* vp;

	printf("char   size %lu\n", sizeof(char));
	printf("double size %lu\n", sizeof(double));
	printf("float  size %lu\n", sizeof(float));
	printf("int    size %lu\n", sizeof(int));
	printf("long   size %lu\n", sizeof(long));
	
	vp = malloc(10);

	free(vp);

	return 0;
}
