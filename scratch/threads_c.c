/**
 * Benchmark to test memory consumption for thread starting
 * @file: threads_c.c
 * Created on Wed Sep 13 09:30:25 2023
 * @author: Geeks For Geeks team
 * From:
 * * https://www.geeksforgeeks.org/multithreading-in-c/
 * An answer to
 * * https://github.com/pkolaczk/async-runtimes-benchmarks
 */

#include <stdio.h>   /* printf() */
#include <stdlib.h>  /* atoi(), calloc(), free() */
#include <unistd.h>  /* sleep() */
#include <pthread.h> /* pthread_t, pthread_create(), pthread_join() */

void *myThreadFun(void *vargp)
{
	/* @obs just to avoid a warning */
	if (vargp != NULL) {
		vargp = (char*)vargp + 1;
	}
	
	sleep(10);
	return NULL;
}

int main(int argc, char* const* argv)
{
	int        n_threads = 1000 * 1000;
	int        thread_count = 0;
	pthread_t* threads;

	if (argc > 1) {
		n_threads = atoi(argv[1]);
	}

	printf("Will create and start %d threads\n", n_threads);
	threads = (pthread_t*)calloc(n_threads, sizeof(pthread_t));
	for (thread_count = 0; thread_count < n_threads; thread_count++) {
		pthread_create(threads + thread_count, NULL, myThreadFun, NULL);
	}
	
	printf("Will join %d threads\n", n_threads);
	for (thread_count = 0; thread_count < n_threads; thread_count++) {
		pthread_join(threads[thread_count], NULL);
	}
	
	free(threads);
	
	/* Normal function termination */
	return 0;
}
