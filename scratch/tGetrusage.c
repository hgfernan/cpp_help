/**
 * @file: tGetrusage.c
 *
 * \brief Sample code for POSIX's getrusage()
 * Created on Fri Sep  8 18:00:51 2023
 * @author: hilton
 */ 

#include <sys/time.h>     /* struct timeval */
#include <sys/stat.h>     
#include <sys/types.h>    /* stat() */
#include <sys/resource.h> /* getrusage(), struct rusage */

#include <wait.h>   /* wait(), waitpid() */
#include <unistd.h> /* fork(), execvp(), getpid(), getppid() */

#include <stdio.h>  /* printf() */
#include <stdlib.h> /* calloc(), free() */
#include <string.h> /* memset(), strlen( */

int main(int argc, char* const* argv)
{
	int    rv;
	int    chld_pid;
	char** chld_argv;
	struct stat   st_buf;
	struct rusage res;
	
	if (argc < 2) {
		printf("%s: ERROR missing child program to execute\n", argv[0]);
		
		/* Return to indicate failure */
		return 1;
	}
	
	memset(&st_buf, 0, sizeof(st_buf));
	
	rv = stat(argv[1], &st_buf);
	if (rv) {
		printf("%s: ERROR stat() returned %d\n", argv[0], rv);
		
		/* Return to indicate failure */
		return 2;		
	}
	
	if (! (st_buf.st_mode & S_IXUSR)) {
		printf("%s: ERROR file '%s' is not executable\n", 
			argv[0], argv[1]);
		
		/* Return to indicate failure */
		return 3;		
	}
	
	/** @internal Create the command line for the child */
	chld_argv = (char**)calloc(argc, sizeof(char*));
	for (int narg = 1; narg < argc; narg++) {
		chld_argv[narg - 1] = 
			(char*)calloc(strlen(argv[narg - 1]) + 1, sizeof(char));
		strcpy(chld_argv[narg - 1], argv[narg]);
	}
	chld_argv[argc - 1] = NULL;
		
	/** @internal Execute the child */
     if ((chld_pid = fork()) < 0) {
        perror("fork() error");
        		
		/* Return to indicate failure */
		return 4;		
        
	 } else if (chld_pid == 0) {
		 printf("Parent id %d, child id %d\n", getppid(), getpid());
		 
		 rv = execvp(argv[1], chld_argv);
		 printf("%s: ERROR Return not expected. execvp() returned %d\n", 
				argv[0], rv);
		
		/* Return to indicate failure */
		return 5;		
     } else {
		 printf("Current id %d\n", getpid());
		 printf("Luke I'm your father\n");
		 		 		 
		 /** Wait for the child */
		 wait(NULL);
		 
		 rv = getrusage(RUSAGE_CHILDREN, &res);
		 
		 if (rv) {
			 perror("getrusage() error");			 
		
			 /* Return to indicate failure */
			 return 6;
		 }
		 
		 printf("Memory usage %ld kb\n", res.ru_maxrss);
	 }
	
	 fflush(stdout);
	 
	/* Normal function termination */
	return 0;
}

