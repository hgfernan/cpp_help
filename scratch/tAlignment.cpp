/**
 * @file: tAlignment.cpp
 * 
 * Showing the alignment of char* blocks and the use of 
 * 	 reinterpret_cast<>
 * Created on Fri Sep  1 16:09:13 2023
 * 
 * @author: hilton  
 */
 
#include <iostream>  // cout, endl

int main() 
{
	char* cp;
	unsigned long ulcp;
	
	cp = new char;
	
	ulcp = reinterpret_cast<unsigned long>(cp);
	
	std::cout << "ulcp " << ulcp 
	          << " remainder of 16 " << (ulcp % 16)
			  << std::endl
	;
	
	delete cp;
	
	/* Normal function termination */
	return 0;
}
