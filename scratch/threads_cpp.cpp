/**
 * Benchmark to test memory consumption for thread starting
 * @file: threads_cpp.cpp
 * Created on Fri Sep  8 16:21:21 2023
 * 
 * @author: hilton
 * * https://github.com/pkolaczk/async-runtimes-benchmarks
 */
 
#include <iostream> // cout, enndl
#include <string>   // stoi()
#include <thread>   // class std::thread, std::this_thread::sleep_for()
#include <vector>

int main(int argc, char* const* argv) 
{
	int n_threads = 1000 * 1000;
	std::vector<std::thread*> threads;

	if (argc > 1) {
		n_threads = std::stoi(argv[1]);
	}
	
	std::cout << argv[0] << ": Will start " << n_threads << " threads"
			  << std::endl;
	
	for (int thread = 0; thread < n_threads; thread++) {
		std::thread* t = new std::thread([](){
				std::this_thread::sleep_for(std::chrono::seconds(10));
			});
		threads.push_back(t);
	}
	
	std::cout << argv[0] << ": Started threads" << std::endl;
	
	for (int thread = 0; thread < n_threads; thread++) {
		threads[thread]->join();
		delete threads[thread];
	}

	std::cout << argv[0] << ": Joined " << n_threads << " threads"
			  << std::endl;
		
	// Normal function termination
	return 0;
}
