/**
 * @file: tVector.cpp
 * 
 * \brief Experiments with vector<>
 * 
 * Created on Fri Sep  8 17:29:57 2023 
 * @author: hilton
 * 
 */

#include <iostream> // cout, endl
#include <vector>   // class vector

int main()
{
	std::vector<char*> vec;
	
	for (int ind = 0; ind < 10; ind++) {
		char* ptr = new char[10];
		std::sprintf(ptr, "%d", ind);
		vec.push_back(ptr);
	}

	std::cout << "Vector size " << vec.size() << std::endl;
	
	for (int ind = 0; ind < 10; ind++) {
		delete vec[ind];
	}

	std::cout << "After deleting elements" << std::endl;
	std::cout << "Vector size " << vec.size() << std::endl;
	
	vec.erase(vec.begin(), vec.end());

	std::cout << "After erasing the vector" << std::endl;
	std::cout << "Vector size " << vec.size() << std::endl;
	
	// Normal function termination
	return 0;
}
