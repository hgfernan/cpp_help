/**
 * @file: tGetrlimit.c
 * Listing of software limits using getrlimit()
 * 
 * Created on Fri Sep 15 15:50:05 2023
 * @author: hilton
 */

#include <sys/time.h>
#include <sys/resource.h>

#include <stdio.h> /* printf() */

#define DIM(x) ( sizeof(x) / sizeof(x[0]) )
#define name_code(x) #x, x

struct name_code_str {
	char* name;
	int code;
};

int main()
{
	int rv;
	unsigned ind;
	struct rlimit limits = {0};
	struct name_code_str codes[] = {
		{ name_code(RLIMIT_AS) }, 
		{ name_code(RLIMIT_CORE) }, 
		{ name_code(RLIMIT_CPU) }, 
		{ name_code(RLIMIT_DATA) }, 
		{ name_code(RLIMIT_FSIZE) }, 
		{ name_code(RLIMIT_LOCKS) }, 
		{ name_code(RLIMIT_MEMLOCK) }, 
		{ name_code(RLIMIT_MSGQUEUE) },
		{ name_code(RLIMIT_NICE) },
		{ name_code(RLIMIT_NOFILE) },
		{ name_code(RLIMIT_NPROC) },
		{ name_code(RLIMIT_RSS) },
		{ name_code(RLIMIT_RTPRIO) },
		{ name_code(RLIMIT_RTTIME) },
		{ name_code(RLIMIT_SIGPENDING) },
		{ name_code(RLIMIT_STACK) }
	};
	
	for (ind = 0; ind < DIM(codes); ind++) {
		char* name = codes[ind].name;
		int code = codes[ind].code;
		
		rv = getrlimit(code, &limits);
		if (rv != 0) {
			printf("*** ERROR could not get the limits for %s (%d)\n",
				name, code);
			continue;
		}
		
		printf("%s (%d)\n\tCurrent %lu, Maximum %lu\n", 
			name, code, limits.rlim_cur, limits.rlim_max);
	}
	
	/* Normal function termination */
	return 0;
}
	
