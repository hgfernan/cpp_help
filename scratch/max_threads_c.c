/**
 * @file: max_threads_c.c 
 * Program to estimate the maximum number of threads using lpthreads
 * 
 * Created on Fri Sep 15 11:20:42 2023
 *
 * @author: @injoy, from stackoverflow.com
 * From: 
 * * https://stackoverflow.com/questions/12387436/the-thread-created-by-pthread-create-the-same-with-the-kernel-thread
 */
 
#include <stdio.h>   /* printf() */
#include <string.h>  /* strerror() */

#include <errno.h>   /* errno */
#include <unistd.h>  /* sleep() */
#include <pthread.h> /* pthread_t, pthread_create(), pthread_join() */

#include <sys/time.h>
#include <sys/resource.h> /* getrlimit() */

#define MAX_THREADS (100 * 1024)

void* thread_sleep(void* arg) {
	int* pseconds = (int*)arg;
	
	sleep(*pseconds);
		    
    /* Normal function termination */
    return NULL;
}

int main()
{
    int rv;
    int err;
    int count = 0;
    int seconds = 10;
    int n_threads = 0;
    pthread_t th_arr[MAX_THREADS];
	struct rlimit limits = {0};

	rv = getrlimit(RLIMIT_STACK, &limits);
	if (rv != 0) {
		int sav_errno = errno;
		printf("*** ERROR could not get limits for RLIMIT_STACK (%d)\n",
			RLIMIT_STACK);
			
		printf("\tErr code %d: %s\n", 
			sav_errno, strerror(sav_errno));
		
		/* Return to indicate failure */
		return 1;
	}
	
	printf("RLIMIT_STACK (%d)\n\tCurrent %lu, Maximum %lu\n", 
		RLIMIT_STACK, limits.rlim_cur, limits.rlim_max);
	
	puts("Will try to change limits");

	limits.rlim_cur *= 2;
	rv = setrlimit(RLIMIT_STACK, &limits);
	if (rv != 0) {
		int sav_errno = errno;
		printf("*** ERROR could not set limits for RLIMIT_STACK (%d)\n",
			RLIMIT_STACK);
			
		printf("\tErr code %d: %s\n", 
			sav_errno, strerror(sav_errno));
			
		/* Return to indicate failure */
		return 2;
	}
	
    while (n_threads < MAX_THREADS) {
        pthread_t pid;
        err = pthread_create(&pid, NULL, thread_sleep, &seconds);
        if (err != 0) {
            break;
        }
        
        th_arr[n_threads] = pid;
        n_threads++;
    }
    printf("Maximum number of threads per process is = %d\n", n_threads);
	
	for (count = 0; count < n_threads; count++) {
        pthread_join(th_arr[count], NULL);
        count++;
    }
    printf("Joined %d threads\n", n_threads);
    
    /* Normal function termination */
    return 0;
}
