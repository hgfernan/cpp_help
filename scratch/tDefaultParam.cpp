/** 
  * @file: tDefaultParam.cpp 
  * Created on Wed 09 Aug 2023 07:35:19 PM -03
  * @author: hilton
  */ 
#include <iostream>
#include <string>

int default_param(std::string s = "", float f = 10.0)
{
	int result = 0;

	if (s.length() == 0) {
		std::cout << "Default string parameter" << std::endl;
		result += 1;
	}

	if (f == 10.0) {
		std::cout << "Default float parameter" << std::endl;
		result += 1;
	}

	// Normal function termination
	return result;
}

int main()
{
	int rv = 0;

	rv = default_param();
	std::cout << "default_param: Returned " << rv << std::endl;

	std::cout << std::endl;
	rv = default_param("a", 2);
	std::cout << "default_param: Returned " << rv << std::endl;

	std::cout << std::endl;
	rv = default_param("b");
	std::cout << "default_param: Returned " << rv << std::endl;

	/// @attention The following lines do not compile
/*
	rv = default_param(11.0);
	std::cout << "default_param: Returned " << rv;

	rv = default_param(float f = 11.0);
	std::cout << "default_param: Returned " << rv;

 */

	// Normal function termination
	return 0;
}

