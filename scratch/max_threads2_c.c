/**
 * @file: max_threads_c.c 
 * Program to estimate the maximum number of threads using lpthreads
 * 
 * Created on Fri Sep 15 11:20:42 2023
 *
 * @author: @injoy, from stackoverflow.com
 * From: 
 * * https://stackoverflow.com/questions/12387436/the-thread-created-by-pthread-create-the-same-with-the-kernel-thread
 */
 
#include <stdio.h>   /* printf() */
#include <errno.h>   /* errno */

#define __USE_POSIX
#include <limits.h>  /* PTHREAD_STACK_MIN */

#include <string.h>  /* strerror() */
#include <unistd.h>  /* sleep() */
#include <pthread.h> /* pthread_t, pthread_create(), pthread_join() */

#define MAXTHREADS 1000000
#define THREADSTACK  PTHREAD_STACK_MIN

void* thread_sleep(void* arg) {
	int* pseconds = (int*)arg;
	
	sleep(*pseconds);
		    
    /* Normal function termination */
    return NULL;
}

int main()
{
    int err;
    int count = 0;
    int seconds = 10;
    int n_threads = 0;
    pthread_t      th_arr[MAXTHREADS];
    pthread_attr_t attrs;

    pthread_attr_init(&attrs);
    pthread_attr_setstacksize(&attrs, THREADSTACK);

    while (n_threads < 1000000) {
        pthread_t pid;
        err = pthread_create(&pid, &attrs, thread_sleep, &seconds);
        if (err != 0) {
			printf("Error %d (%s)\n", errno, strerror(errno));	
            break;
        }
        
        th_arr[n_threads] = pid;
        n_threads++;
    }
    printf("Maximum number of threads per process is = %d\n", n_threads);
	
	for (count = 0; count < n_threads; count++) {
        pthread_join(th_arr[count], NULL);
        count++;
    }
    printf("Joined %d threads\n", n_threads);
    
    /* Normal function termination */
    return 0;
}
