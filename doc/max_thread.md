# Experiences trying to maximize the number of threads

## Experiences

* Tried increasing the thread stack according to 
    [What is the maximum number of threads that pthread_create can create? [duplicate]](https://stackoverflow.com/questions/12387828/what-is-the-maximum-number-of-threads-that-pthread-create-can-create?utm_source=pocket_reader) 

    Unfortunately, it didn't work, and it doesn't seem to make sense, as the problem seems to be in the process sizes, not the thread size;

* Tried to increment the size of virtual memory according to 
    [Maximum Number of Threads per Process in Linux](https://www.baeldung.com/linux/max-threads-per-process)

* Then tried to increment the number of valid `pid`'s, according to 
    [Get and Set Max Thread Count in Linux](https://linuxhint.com/get-set-max-thread-count-linux/)


## Results

In general, the maximum number of threads obtained is around 30k, below the nominal values the parameters report. It seems that increasing the maximum number of `pid` values had a more important result, but only that.

