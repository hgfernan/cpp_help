#include <iostream>
#include <cstdlib>

#include "tgCmdline.h"

int main(int argc, char *argv[])
{
    gengetopt_args_info ai;

    if (cmdline_parser (argc, argv, &ai) != 0) {
        exit (1);
    }

    if (ai.debug_flag) {
        std::cout << "Option --debug was informed" << std::endl;
    }

    if (ai.file_given) {
        std::cout << "Option --file was informed: '"
		  << ai.file_arg << "'"
		  << std::endl;
    }

    if (ai.times_given) {
        std::cout << "Option --times was informed: '"
		  << ai.times_arg << "'"
		  << std::endl;
    }

    return 0;
}
