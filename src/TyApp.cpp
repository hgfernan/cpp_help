/**
  * \todo create the file header
  *
  */

#include "YaCsvParser.hpp"
#include "TyApp.hpp"

#include <cxxopts.hpp>

TyApp::TyApp (const int argc, char** argv)
{
	this->parsed = false;
	
	this->argc = argc;
	this->argv = argv;
	
	this->debug = false;
	this->echo  = false;
	this->terse = false;
	
	this->basFileName = 
	this->inpFileName = 
	this->outFileName = 
	this->repFileName = "";
	
	this->fieldSep = 
	this->quote    = "";
}

bool
TyApp::parseCmdLine ()
{
	bool result = true;
	
	cxxopts::Options options ("tYacCsvParser", "Test of class YacCsvParser, yet another parser of CSV files.");

	
	options.add_options ()
		( "h,help" , "Help listing" )
		( "b,basement", "Basement file, to store debugging. Implies -d", 
			cxxopts::value<std::string> ())
		( "d,debug", "Enable debugging" )
		( "e,echo", "Echo fields parsed for each line" )
		( "i,input" , "Input file name", 
			cxxopts::value<std::string> ()->default_value (std::string ("")) )
		( "o,output" , "Output file name for human readable reports. Implies -e", 
			cxxopts::value<std::string> ())
		("q,quote", "Quote string", 
			cxxopts::value<std::string> ()->default_value (YaCsvParser::DEF_QUOTE))
		( "r,report" , "Report file name for computer readable forms. Implies -t", 
			cxxopts::value<std::string> ())
		("s,separator", "Field separator string", 
			cxxopts::value<std::string> ()->default_value (YaCsvParser::DEF_FLDSEP))
		( "t,terse", "Enable terse mode -- suitable for spreadsheets" )
		;

	cxxopts::ParseResult params = options.parse (this->argc, this->argv);
	

	/** \todo handle --debug option */
	/** \todo handle --help option */
	
	/** \todo handle exceptions due to wrong options */
	/** \todo handle repeated use of options */

	result = true;
	
	this->inpFileName = params["input"].as<std::string> ();
	this->fieldSep = params["separator"].as<std::string> ();
	this->quote = params["quote"].as<std::string> ();
	
	if (params["help"].count () > 0)
	{
		std::cout << options.help () << std::endl;
		
		exit (1);
	}
	
	if (params["debug"].count () > 0)
	{
		this->debug = true;
	}
	
	if (params["echo"].count () > 0)
	{
		this->echo = true;
	}
	
	if (params["terse"].count () > 0)
	{
		this->terse = true;
	}
	
	if (params["basement"].count () > 0) 
	{
		this->basFileName = params["basement"].as<std::string> ();
		this->debug = true;
	}
	
	if (params["output"].count () > 0) 
	{
		this->outFileName = params["output"].as<std::string> ();
		this->echo = true;
	}
	
	if (params["report"].count () > 0) 
	{
		this->repFileName = params["report"].as<std::string> ();
		this->terse = true;
	}
	
	// Normal function termination
	return result;
}

bool
TyApp::interpretParams () 
{
	bool result;
	
	/** \todo open input file here, if not given */
	
	/** \todo handle opening errors for output files */
	result = true;
	
	if (this->basFileName != "")
	{
		this->basStream.open (this->basFileName);
	}
	
	if (this->outFileName != "")
	{
		this->outStream.open (this->outFileName);
	}

	if (this->repFileName != "")
	{
		this->repStream.open (this->repFileName);
	}
	
	// Normal function termination
	return result;	
}

std::size_t 
TyApp::basWrite (const std::string& buf)
{
	std::size_t result = 0;
	
	// Will not write anything if debug is not enabled
	if (! this->debug) 
	{
		return 0;
	}
	
	// If output stream is not open, write to cerr
	if (this->basStream.is_open ())
	{
		this->basStream << buf;
		this->basStream.flush ();
	}
	else 
	{
		std::cerr << buf;
		std::cerr.flush ();
	}
	
	result = buf.length ();
	
	return result;
}

std::size_t 
TyApp::outWrite (const std::string& buf)
{
	std::size_t result = 0;
	
	// Will not write anything if echo is not enabled
	if (! this->echo) 
	{
		return 0;
	}
	
	// If output stream is not open, write to cout
	if (this->outStream.is_open ())
	{
		this->outStream << buf;
		this->outStream.flush ();
	}
	else 
	{
		std::cout << buf;
		std::cout.flush ();
	}
	
	result = buf.length ();
	
	return result;
}

std::size_t 
TyApp::repWrite (const std::string& buf)
{
	std::size_t result = 0;
	
	// Will not write anything if report is not enabled
	if (! this->terse) 
	{
		return 0;
	}
	
	// If report stream is not open, write to cout
	if (this->repStream.is_open ())
	{
		this->repStream << buf;
		this->repStream.flush ();
	}
	else 
	{
		std::cout << buf;
		std::cout.flush ();
	}
	
	result = buf.length ();
	
	return result;
}

static TyApp *__global = (TyApp*)NULL;

TyApp* 
setGlobal (TyApp* app)
{
	/** \todo parameter checking */
	TyApp* result = app;
	
	__global = result = app;
	
	return result;
}

TyApp* 
getGlobal ()
{
	TyApp* result = __global;
	
	return result;	
}
