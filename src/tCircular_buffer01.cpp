/**
  Simple use of circular buffer: do not trust the iterators; they're moving

  Created on Tue 25 Jul 2023 05:37:37 PM -03

  @author: boost.org documenters

  From:
  * https://www.boost.org/doc/libs/1_81_0/doc/html/circular_buffer/implementation.html
  */

#include <iostream> // cout, endl

// OBS This adds the careful use of checking
#define  BOOST_CB_ENABLE_DEBUG 1

#include <boost/circular_buffer.hpp>
#include <boost/assert.hpp>
#include <assert.h>

// TODO should return an iostream
template <typename  T>
int print_cb (boost::circular_buffer<T> cb)
{
    for (boost::circular_buffer<int>::iterator it = cb.begin(); it != cb.end(); it++) {
        std::cout << *it << " ";
    }

    std::cout << std::endl;

    // Normal function termination
    return 0;
}

int main(int /*argc*/, char* /*argv*/[])
{

  boost::circular_buffer<int> cb(3);

  cb.push_back(1);
  cb.push_back(2);
  cb.push_back(3);

  boost::circular_buffer<int>::iterator it = cb.begin();
  int val = *it;
  std::cout << "After 3 assignments: " << std::endl;
  print_cb(cb);

  std::cout << "*cb.begin() " << val << std::endl;

  std::cout << "Will assert (*it == 1)" << std::endl;
  assert(*it == 1);
  std::cout << "Passed\n" << std::endl;

  std::cout << "Will push back 4"<< std::endl;
  cb.push_back(4);
  print_cb(cb);

  std::cout << "Will get cb.begin() again"<< std::endl;
  it = cb.begin();
  val = *it;
  std::cout << "After 4 assignments: " << "*cb.begin() " << val << std::endl;

  std::cout << "Will assert (*it == 2)" << std::endl;
  assert(*it == 2); // The iterator still points to the initialized memory.
  std::cout << "Passed\n" << std::endl;

  return 0;
}
