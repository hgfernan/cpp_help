#include <iostream>
#include <cstring>  // memset()
#include <ctime>    // time(), localtime(), strftime()
#include <cstdlib>  // systemn()
#include <cstdio>   // printf(), setbuf() 

// TODO replace references to C's stdio.h to iostream

#include <unistd.h>    // _SC_CLK_TCK, sysconf(), 
#include <sys/times.h> // struct tms, times()

/**
 * 
 * From<br/> 
 * <a href="https://stackoverflow.com/questions/5838156/differences-between-real-time-user-time-and-system-time">
 * Differences between real time, user time, and system time [duplicate]
 * </a> by 
 * <a href="https://stackoverflow.com/users/404020/node-ninja"> node ninja </a>
 * 
 */
 
using namespace std;


class RusTimer
{
private:
	static clock_t _clocksPerSec;

	bool started;
	bool stopped;
	clock_t clkStart;
	clock_t clkStop;
	struct tms tmsStart;
	struct tms tmsStop;
	
	double rTime;
	double uTime;
	double sTime;
	double cuTime;
	double csTime;
	
public:
	RusTimer (void);
	clock_t start ();
	clock_t stop ();
	bool wasStopped () {return this->stopped;};	
	bool wasStarted () {return this->started;};
	
	double getStartTime (); 
	double getStopTime (); 
	double getRealTime ();
	double getUserTime ();
	double getSysTime ();
	double getChildUserTime ();
	double getChildSysTime ();
};

clock_t RusTimer::_clocksPerSec = 0;

RusTimer::RusTimer (void) 
{
	/* fetch clock ticks per second first time */
	if (RusTimer::_clocksPerSec == 0) {
		RusTimer::_clocksPerSec = sysconf (_SC_CLK_TCK);
		
		if (RusTimer::_clocksPerSec < 0)
			cout << "sysconf error" << endl;
		
		// TODO raise an exception
	}
	
	this->started  = 
	this->stopped  = false;
	this->clkStart = 
	this->clkStop  = 0;
	memset (&this->tmsStart, 0, sizeof (struct tms));
	memset (&this->tmsStop, 0, sizeof (struct tms));
	
	rTime  = 
	uTime  = 
	sTime  = 
	cuTime = 
	csTime = 0.0;

	return;
}

clock_t RusTimer::start ()
{
	clock_t result;
	result = this->clkStart = times (&this->tmsStart);
	
	if (-1 != result) 
	{
		this->started = true;
	}
	
	return result;
}

clock_t RusTimer::stop ()
{
	clock_t result;
	result = this->clkStop = times (&this->tmsStop);
	
	if (-1 == result) 
	{
		cout << "sysconf error" << endl;
		
		// Return to indicate failure
		return result;		
	}
	
	this->stopped = true;
	
	this->rTime  = (this->clkStop - this->clkStart)
	             / (double)RusTimer::_clocksPerSec;
	this->uTime  = (this->tmsStop.tms_utime  - this->tmsStart.tms_utime)
				 / (double)RusTimer::_clocksPerSec;
	this->sTime  = (this->tmsStop.tms_stime  - this->tmsStart.tms_stime)
	             / (double)RusTimer::_clocksPerSec;
	this->cuTime = (this->tmsStop.tms_cutime - this->tmsStart.tms_cutime)
	             / (double)RusTimer::_clocksPerSec;
	this->csTime = (this->tmsStop.tms_cstime - this->tmsStart.tms_cstime)
	             / (double)RusTimer::_clocksPerSec;
	
	return result;
}

double RusTimer::getStartTime () 
{
	double result;
	
	if (this->clkStart == 0) {
		cout << "Stopwatch was not started" << endl;
		
		// TODO raise an exception		
	}
	
	result = double (this->clkStart) / RusTimer::_clocksPerSec;
	
	return result;
}

double RusTimer::getRealTime () {
	double result;
	
	if (not this->stopped) {
		cout << "Stopwatch was not stopped" << endl;
		
		// TODO raise an exception		
	}
	
	result = this->rTime;
	
	return result;
}

double RusTimer::getUserTime () {
	double result;
	
	if (not this->stopped) {
		cout << "Stopwatch was not stopped" << endl;
		
		// TODO raise an exception		
	}
	
	result = this->uTime;
	
	return result;
}

double RusTimer::getSysTime () {
	double result;
	
	if (not this->stopped) {
		cout << "Stopwatch was not stopped" << endl;
		
		// TODO raise an exception		
	}
	
	result = this->sTime;
	
	return result;
}

double RusTimer::getChildUserTime () {
	double result;
	
	if (not this->stopped) {
		cout << "Stopwatch was not stopped" << endl;
		
		// TODO raise an exception		
	}
	
	result = this->cuTime;
	
	return result;
}

double RusTimer::getChildSysTime () {
	double result;
	
	if (not this->stopped) {
		cout << "Stopwatch was not stopped" << endl;
		
		// TODO raise an exception		
	}
	
	result = this->csTime;
	
	return result;
}

int 
printTimes (RusTimer* timer, const char* command) 
{
	char timeStamp[30];
	size_t result;
	time_t now;
	struct tm *tmNow;
	
	now = time (NULL);
	tmNow = localtime (&now);
	
	result = strftime (timeStamp, sizeof (timeStamp), 
	                   "%Y-%m-%d, %H:%M:%S", 
	                   tmNow
	                  );
	
	if (!result) 
	{
		return result;
	}
	
	// CSV timestamp, real, user, sys, child user, child sys, "command"
	result = 
	printf ("%s, %7.3f, %7.3f, %7.3f, %7.3f, %7.3f, '%s'\n", 
	        timeStamp, 
	        timer->getRealTime (), timer->getUserTime (), timer->getSysTime (),
	        timer->getChildUserTime (), timer->getChildSysTime (), 
	        command
	       );

	return result;
}

/** 
 * 
 * execute and time the "cmd" 
 * 
 */
 
static int
doCommand (const char *command)
{
  int status;

//  printf ("\ncommand: %s\n", command);
  RusTimer *timer = new RusTimer ();
  
  /* starting timer */
  timer->start ();

  if (! timer->wasStarted ()) 
  {	
    cout << "starting error" << endl;
    
    // Return to indicate failure
    return 1;
  }
  
  /* execute command */
  status = system (command);

  if (status < 0)
  {	
    cout << "system error" << endl;
    
    // Return to indicate failure
    return 1;
  }
  
  timer->stop ();

  if (! timer->wasStopped ()) 
  {
    cout << "times error" << endl;
    
    // Return to indicate failure
    return 1;
  }
  
  printTimes (timer, command);
  
  delete timer;
  
  // Return the status of the called child
  return status;
}

int
main (int argc, char* const* argv)
{
  int nArg;
  setbuf (stdout, NULL);
  
  for (nArg = 1; nArg < argc; nArg++) 
  {
    doCommand (argv[nArg]);		/* once for each command-line arg */
  }
  
  return 0;
}
