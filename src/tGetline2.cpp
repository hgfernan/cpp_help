/**
  * istream::getline example
  * Slightly adapted from
  * <ol>
  * 	<li>
  * 		<a href="http://www.cplusplus.com/reference/istream/istream/getline/">
  * 			std::istream::getline
  * 		</a>
  * 	</li>
  * 	<li>
  * 		<a href="http://www.cplusplus.com/reference/istream/istream/gcount/">
  * 			std::istream::gcount
  * 		</a>
  * 	</li>
  * </ol> 
  * 
  */ 

// istream::getline example

#include <iostream>  // std::cin, std::cout, std::cin.gcount()

int main () {
  char line[256];
	

  std::cout << "Please, enter several lines, and end them with ^D -- Posix EOF char." << std::endl;
  
  std::cin.getline (line, sizeof (line) - 1);
  while (! std::cin.eof ())
  {
	std::cout << std::cin.gcount () << ": " << line << std::endl;
	std::cin.getline (line, sizeof (line) - 1); 
  }
  return 0;
}
