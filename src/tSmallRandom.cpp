#include <iostream> // std::cout, std::endl
#include <ctime>    // time()
#include <cstdlib>  // srandom(), random()

int 
mySmallRandom () 
{
	int result;

	result = (random () % 32768);
	
	// Normal function termination 
	return result;
}

int 
is3Divisible (int n) 
{
	return (n % 3) == 0;
}

int 
main ()
{
	int    n;
	int    nTry;
	int    nSuccesses;
	int    totalN;
	double avg;
	const int nTRIES = (100 * 1000);

	srandom (time (NULL));

	nSuccesses = 0;
	totalN     = 0;

	for (nTry = 0; nTry < nTRIES; nTry++)
	{
		n       = mySmallRandom ();
		totalN += n;
	
		if (is3Divisible (n) || is3Divisible (n + 1) || is3Divisible (n + 2))
		{
			nSuccesses++;	
		}
	}	

	avg = (totalN * 1.0) / nTRIES;

	std::cout << "number of tries: " << nTRIES << std::endl;
	std::cout << "average n:       " << avg    << std::endl;	
	std::cout << nSuccesses << " successes in " <<  nTRIES << std::endl;
	
	// Normal function termination
	return 0;
}
