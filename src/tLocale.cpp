#include <iostream>
#include <locale>

/**
 * Sligthly adapted from 
 * <ul>
 * 	<li>
 * 		<a href="http://en.cppreference.com/w/cpp/locale/locale">
 * 			std::locale - cppreference.com
 * 		</a><br/>
 * 		OBS: uses wcout instead of cout, to allow Unicode locales.
 * 	</li>
 * </ul>
 *
 * 
 */
 
int main ()
{
	// print preferred locale setting 
    std::wcout << "User-preferred locale setting is " << std::locale ("").name ().c_str () << '\n';
    
    // on startup, the global locale is the "C" locale, not the user's preferred one
    std::wcout << 1000.01 << '\n';
    
    // replace the C++ global locale as well as the C locale with the user-preferred locale
    std::locale::global (std::locale (""));
    
    // use the new global locale for future wide character output
    std::wcout.imbue (std::locale ());
    
    // output the same number again
    std::wcout << 1000.01 << '\n';
    
    return 0;
}
