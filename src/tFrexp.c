/**
  * Slightly changed from<br>
  * <ol>
  *	<li>
  *		man:frexp
  *	</li>
  * </ol> 
  *
  */
#include <math.h>
#include <float.h>
#include <stdio.h>
#include <stdlib.h>

int
main ()
{
	double x, r;
	int exp;
	int n;

	n = 1;
	for (int i = 0; i < 10; i++) 
	{
		n *= 10;
		x = (double)(n - 1); 
		r = frexp (x, &exp);

		printf ("frexp(%10.0f, &e) = %.6f: %.6f * %d^%02d = %10.0f\n",
	       		 x, r, r, FLT_RADIX, exp, x);
	}

	return 0; 
}

