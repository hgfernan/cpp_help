#include <string>
#include <iostream>

struct SingularPlural {
	string singular;
	string plural;
};

SingularPlural several[] = {{"test", "tests"}, {"example", "examples"}};
