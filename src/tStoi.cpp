#include <string>    // stoi()
#include <iostream> 

#include <cctype>    // isdigit()

bool
isDecimal (const std::string& s) 
{
	bool result = true;
	
	for (std::size_t ind = 0; ind < s.length (); ind++)
	{
		if (! isdigit (s[ind]) )
		{
			result = false;
			break;
		}
	}
	
	return result;
}

void 
testDecimal (const std::string& s)
{
	std::cout << "The string '" << s << "' "; 
	
	if (isDecimal (s))
	{
		std::cout << "converted to a decimal integer is " 
				  << std::stoi (s) << "\n";
	} 
	else 
	{
		std::cout << "cannot be converted to a decimal integer.\n";
	}
}

int 
main (void)
{
	testDecimal ("aaaaa");
	testDecimal ("10");
	testDecimal ("a10aa");
	testDecimal ("20");
	testDecimal (" 10 ");
	  
	return 0;
}
