/**
  * \todo create the file header
  *
  */
  
#include "YaCsvParser.hpp"

std::string 
charToEscape (const char c) 
{
	switch (c) 
	{
	case '\0':
		return "\\0";
	case '\a':
		return "\\a";
	case '\b':
		return "\\b";
	case '\f':
		return "\\f";
	case '\n':
		return "\\n";
	case '\r':
		return "\\r";
	case '\t':
		return "\\t";
	case '\v':
		return "\\v";
	case '\\':
		return "\\\\";
	case '"':
		return "\\\"";
	case '\'':
		return "\\'";
	}

	/** \todo include all non printable characters */
	/** \todo what about UNICODE */
	
	return std::string (sizeof (char), c);
}

std::string 
escapeString (const std::string& str)
{
	std::size_t ind;
	std::string result;
	
	for (ind = 0; ind < str.length (); ind++)
	{
		result += charToEscape (str[ind]);
	}
	
	return result;
}

std::string trim ( const std::string& input, const std::string& delim = " " ) 
{
	std::size_t currInd;
	std::string result = input;
	
	currInd = result.find_first_not_of (delim);
	if ((currInd != 0) && (currInd != std::string::npos))
	{
		result = result.substr (currInd);
	}
	
	currInd = result.find_last_not_of (delim);
	if (( currInd != (result.length ()) ) &(currInd != std::string::npos)) 
	{
		result = result.substr (0, currInd + 1);
	}
	
	// Normal function termination
	return result;
}

bool
isDecimal (const std::string& s) 
{
	bool result = true;
	
	for (std::size_t ind = 0; ind < s.length (); ind++)
	{
		if (! isdigit (s[ind]) )
		{
			result = false;
			break;
		}
	}
	
	return result;
}

/** \todo use templates for generic transformation */

std::string 
strVectorToString (std::vector <std::string> input) 
{
	std::string result ("[ ");
	
	if (input.size () > 0)
	{
		for (std::string str : input ) 
		{
			result += "'" + escapeString (str) + "', ";
		}
		
		result = result.substr (0, result.length () - 2);
    }
	result += " ]";
	
	return result;
}

/** \todo use templates for generic transformation */

std::string 
size_tVectorToString (std::vector <std::size_t> input) 
{
	std::string result ("[ ");
	
	if (input.size () > 0)
	{
		for (size_t val : input ) 
		{
			result += std::to_string (val) + ", ";
		}
		
		result = result.substr (0, result.length () - 2);
    }
	result += " ]";
	
	return result;
}

const std::string YaCsvParser::DEF_FLDSEP = ";";   /**< Default buffer field separator */
const std::string YaCsvParser::DEF_LINESEP = "\n"; /**< Default buffer line separator */
const std::string YaCsvParser::DEF_QUOTE = "'";    /**< Default quote character */
const std::size_t YaCsvParser::BUF_LEN = (1024 * 1024);  /**< Default buffer length */
const std::size_t YaCsvParser::BUF_INC = (BUF_LEN / 10); /**< Default buffer growth increment */

/** \todo create a workhorse function to do basic initialization */

YaCsvParser::YaCsvParser () 
{
	this->parsed  = false;
	this->fldSep  = DEF_FLDSEP;
	this->lineSep = DEF_LINESEP;
	this->quote   = DEF_QUOTE;
	this->maxLine = BUF_LEN - DEF_FLDSEP.length ();
	this->recBuf  = new char[BUF_LEN];
	this->recBufSize = BUF_LEN;
	this->recBufUsed = 0;
	
	this->app = TyApp::getGlobal ();
	std::cerr << "this->app: " << this->app << std::endl;
	std::cerr.flush ();
	
	return;
}

YaCsvParser::YaCsvParser (std::string fldSep, std::string quote) 
{
	this->parsed  = false;
	this->fldSep   = fldSep;
	this->lineSep = DEF_LINESEP;
	this->quote   = quote;
	this->maxLine = BUF_LEN - DEF_FLDSEP.length ();
	this->recBuf  = new char[BUF_LEN];
	this->recBufSize = BUF_LEN;
	this->recBufUsed = 0;
	
	return;
}

YaCsvParser::YaCsvParser (std::string inpName) 
{
	this->parsed  = false;
	this->fldSep  = DEF_FLDSEP;
	this->lineSep = DEF_LINESEP;
	this->quote   = DEF_QUOTE;
	this->maxLine = BUF_LEN - DEF_FLDSEP.length ();
	this->recBuf  = new char[BUF_LEN];
	this->recBufSize = BUF_LEN;
	this->recBufUsed = 0;
	
	// TODO handle exceptions
			
	this->inpStream.open (inpName, std::ifstream::in);
	  
	return;
}

YaCsvParser::~YaCsvParser ()
{
	if (inpStream.is_open ()) 
	{
		inpStream.close ();
	}
	
	delete[] this->recBuf;
}

bool 
YaCsvParser::setInputName (std::string inpName)
{
	// TODO handle exceptions
			
	this->inpStream.open (inpName, std::ifstream::in);
	
	return this->inpStream.is_open ();
}

bool 
YaCsvParser::goNextLine ()
{
	if (! inpStream.is_open ())
	{
		return false;
	}
	
	if (! inpStream.good ())
	{
		return false;
	}
	
	inpStream.getline (this->partBuf, BUF_LEN);
	
	if (this->inpStream.eof ())
	{
		return false;
	}
	
	// A line was read, but still not parsed
	this->parsed = false;
	
	std::size_t nRead = inpStream.gcount ();
	std::size_t nTransfer = strlen (this->partBuf);
	
	// A whole line was read, either by line sep match, or EOF
	if ((nRead <= BUF_LEN) || (nTransfer < nRead))
	{
		this->recBufUsed = nTransfer;
		strncpy (this->recBuf, this->partBuf, nTransfer);
		return true;
	}
	
	// The reading buffer was not enough to hold the line, will read more
	this->recBufUsed = nTransfer; // To be incremented with new line fragment
	char* newBuf = new char[BUF_LEN + BUF_INC];
	strncpy (newBuf, this->partBuf, BUF_LEN);
	this->recBufSize = BUF_LEN + BUF_INC;
	
	delete[] this->recBuf;
	this->recBuf = newBuf;
	
	/** \todo read one more line fragment */
	inpStream.getline (this->partBuf, BUF_INC);
	
	nRead     = inpStream.gcount ();
	nTransfer = strlen (partBuf);
	
	strncat (this->recBuf, this->partBuf, BUF_INC);
	this->recBufUsed += nTransfer; // Incremented with new line fragment
	
	/** \todo implement a loop to go after the end of line some more MBs of memory */
	
	return true;
}

std::vector <std::string> 
YaCsvParser::getFields ()
{
	std::size_t decrement;
	std::size_t first, second;
	std::size_t fldSepLen;
	std::size_t quoteLen;
	std::size_t currInd;
	std::size_t indQuote;
	std::size_t indSep;
	std::string strBuf;
	std::string aField;
	std::string fldDelim;
	std::string srchStr (this->recBuf);
	std::vector <std::size_t> toInv;
	std::vector <std::size_t> fldPos;
	std::vector <std::size_t> quotePos;
	std::vector <std::size_t>::iterator iter;
	std::vector <std::pair <std::size_t, std::size_t>> quotePair;
	
	/** @todo See what to do with \t and ' ' since both can be 
	 *        field separators.
	 */
	 
	/** 
	  * @todo Check state of string and when to return an empty vector
	  * 
	  */
	 
	this->fields.clear ();
	
//	std::cerr << "Input string: '" << escapeString (srchStr) << "'" << std::endl; 
// #if 0
// 	strBuf = "Input string: '" + escapeString (srchStr) + "'\n"; 
// 	app->basWrite (strBuf);
	
//	std::cerr << "Input string length: " << srchStr.length () << std::endl; 
// 	strBuf = "Input string length: " + std::to_string (srchStr.length ()) 
// 	       + "\n"; 
// 	app->basWrite (strBuf);
// #endif /* 0 */
	
	/** Deblank string from the left */	   
	/** Deblank string to the right */
	srchStr = trim (srchStr);

// #if 0	
// //	std::cerr << "Deblanked string : '" 
// //			  << escapeString (srchStr) << "'"
// //			  << std::endl; 
// 	strBuf = "Deblanked string : '" + escapeString (srchStr) 
// 	       + "'\n"; 
// 	app->basWrite (strBuf);
// #endif /* 0 */
	
	/** Mark the fld seps position, but don't break record */
	currInd = 0;
	fldSepLen = this->fldSep.length ();
	
	currInd = srchStr.find (this->fldSep.c_str (), 0, fldSepLen);
	std::cerr << "fldSep: '" << this->fldSep.c_str () << "'" << std::endl;
	std::cerr << "fldSepLen: " << fldSepLen << "" << std::endl;
	while (currInd != std::string::npos) 
	{
		fldPos.push_back (currInd);
		currInd = srchStr.find (this->fldSep.c_str (), currInd + 1, fldSepLen);
	}
	std::cerr << "Field separator positions: " 
			  << size_tVectorToString (fldPos) 
			  << std::endl; 
	
	/** Find all quotes */
	currInd = 0;
	quoteLen = this->quote.length ();
	
	currInd = srchStr.find (this->quote.c_str (), 0, quoteLen);
	while (currInd != std::string::npos) 
	{
		quotePos.push_back (currInd);
		currInd = srchStr.find (this->quote.c_str (), currInd + 1, quoteLen);
	}
	std::cerr << "Quote separator positions: " 
			  << size_tVectorToString (quotePos) 
			  << std::endl; 
		
	/** Generate pairs */
	for (indQuote = 0; 
		 indQuote < (quotePos.size() & (~1)); // To get largest even < size
		 indQuote += 2
		)
	{
		first  = quotePos[indQuote];
		second = quotePos[indQuote + 1];
		quotePair.push_back (std::make_pair (first, second));		
	}
	
	/** Handle odd number of quotes */
	if (quotePos.size () % 2) 
	{
		first  = quotePos[quotePos.size () - 1];
		second = std::string::npos;
		quotePair.push_back (std::make_pair (first, second));
	}
	
	/** \todo invalidate separators between quotes */
	indQuote = 0;
	toInv.clear ();
	for (indSep = 0; 
		 ( (indSep < fldPos.size ()) && (indQuote < quotePair.size ()) ); 
		 indSep++) 
	{
		// go next, if sep precedes quote pair 
		if (fldPos[indSep] < quotePair[indQuote].first)
		{
			continue;
		}
		
		// find quote pair that's nearest to field separator succedes quote pair
		for (; indQuote < quotePair.size (); indQuote++) 
		{
			if (quotePair[indQuote].second > fldPos[indSep]) 
			{
				break;
			}
		}
		
		if ( (indQuote < quotePair.size ()) && (quotePair[indQuote].first < fldPos[indSep]) )
		{
			toInv.push_back (indQuote);
		}
	}
	
	/** Invalidate separators if they're between quotes */
	decrement = 0;
	for (iter = toInv.begin (); iter != toInv.end (); iter++) 
	{
		fldPos.erase (quotePos.begin () + (*iter - decrement));
		decrement++;
	}
	
	/** Break the fields and attribute them to vector elements */
	fldDelim  = std::string (" \t");
	std::cerr << "Delim string: '" << escapeString (fldDelim) << "'\n";
	currInd = 0;
	for (iter = fldPos.begin (); iter < fldPos.end (); iter++) 
	{
		aField = srchStr.substr (currInd, (*iter - currInd));
		
		std::cerr << "Before deblank: aField: '" << escapeString (aField) << "'\n";
		aField = trim (aField, fldDelim);
		std::cerr << "After deblank: aField: '" << escapeString (aField) << "'\n";
		
		if (aField.length () > 2 * quoteLen)
		{
			std::size_t quoteLen  = this->quote.length ();
			std::size_t remain = aField.length () - quoteLen;
			
			if ((aField.substr (0, quoteLen) == quote) &&
			    (aField.substr (remain, quoteLen) == quote)
			   )
			{
				aField = aField.substr (quoteLen);
				remain = aField.length () - quoteLen;
				aField = aField.substr (0, remain);
			}
		}
		
		this->fields.push_back (aField);
		
		currInd = *iter + this->fldSep.length ();
	}
	
	std::cerr << "After the loop: currInd: " << currInd << "\n";
	
	// Include the remains of the input string
	if (currInd <= (srchStr.length () - 1)) 
	{
		aField = 
			srchStr.substr (currInd);
		
		std::cerr << "Before deblank: aField '" << escapeString (aField) << "'\n";
		/** Deblank string from the left */	   
		/** Deblank string to the right */
		aField = trim (aField, fldDelim);
		
		std::cerr << "After deblank: aField '" << escapeString (aField) << "'\n";
		
		this->fields.push_back (aField);	
	}
	
	// Set the flag to indicate record was parsed
	this->parsed = true;
	
	// Return the fields just calculated
	return this->fields;
}

std::string 
YaCsvParser::getField (std::size_t index)
{
	// if the current line was not parsed, let's do it.
	if (! this->parsed) 
	{
		this->getFields ();
	}
	
	if (index < this->fieldCount ()) 
	{
		// Return the field that was asked for
		return this->fields[index];
	}
	
	/** \todo Handle invalid indices with exceptions */
	
	// Return to indicate failure
	return std::string ("");
}

std::size_t 
YaCsvParser::fieldCount ()
{
	if (! this->parsed) 
	{
		this->getFields ();
	}
		
	// Normal function termination
	return this->fields.size ();
}
