/**
 * @file expreSequence.cpp
 * <br/> 
 * <ol>
 * 		<li>
 * 			<a href="https://stackoverflow.com/questions/6281461/enum-to-string-c">
 * 				Enum to String C++ [duplicate]
 * 			</a>
 * 		</li>
 * 		<li>
 * 			<a href="http://www.cplusplus.com/reference/map/map/operator[]/">
 * 				std::map::operator[] -- C++ Reference
 * 			</a>
 * 		</li>
 * 		<li>
 * 			<a href="http://www.cplusplus.com/reference/map/map/find/">
 * 				std::map::find -- C++ Reference
 * 			</a>
 * 		</li>
 * </ol>
 */
 
#include <map>       //< template map <>
#include <vector>
#include <string>
#include <iostream>

#include <cstdlib> // srand(), rand()

const int MY_RAND_MAX = 32768; 
const int UNARY_MINUS = (MY_RAND_MAX / 4); 

#if defined(TEST)
	const int TOT_EXPR    = 10; //< Number of expressions
	const int TOT_MAX_OPS = 5;  //< Maximum number of operators per line
	
#else /* !defined(TEST) */
	const int TOT_EXPR    = (10 * 1000); //< Number of expressions
	const int TOT_MAX_OPS = 20;          //< Maximum number of operators per line
	
#endif /* TEST */

std::string trim (const std::string& in);

enum Operation
{
	opAdd, opDiv, opMult, opSub, 
	OpFIRST = opAdd,  //< First operation 
	opLAST  = opSub,  //< Last operation
};

std::string 
operationToString (Operation op) 
{
	/** @todo parameter checking */
	
	std::cout << "op == " << int (op) << std::endl;
	
	std::string result 
		((const char *[]) 
		 {
			"+",
			"/", 
			"*",
			"-",
		 }[op]);
	
	// Normal function termination
	return result;
}

Operation 
stringToOperation (const std::string& text)
{
	Operation   result;
	std::string aux = text;
	std::map <std::string, Operation>           map;
	std::map <std::string, Operation>::iterator it;
	
	/** @todo transfer this to a initialization step */
	/** @todo what about a look up table ? */
	
	for (Operation op = Operation::OpFIRST; op <= Operation::opLAST; op = Operation (int (op) + 1))
	{
		map[operationToString (op)] = op; 
	}
	
	std::cout << "before: text == '" << aux << "'" << std::endl;  
	aux = trim (aux);
	std::cout << "after:  text == '" << aux << "'" << std::endl;
	
	it = map.find (aux);

	if (it == map.end ())
	{
		return (Operation)(-1);
	}

	result = it->second;
	
	// Normal function termination
	return result;
}

std::string 
trim (const std::string& in)
{
	std::string            result = in;
	std::string::size_type pos;
	
	// Trim to left
	pos = result.find_first_not_of (" \t");
	
	std::cout << "entered -- pos: " << pos 
			  << ", result:  '" << result << "'" 
			  << std::endl;
	
	if (pos == std::string::npos)
	{
		return std::string ("");
	}
	
	result = result.substr (pos);
	}
	
	std::cout << "left -- pos: " << pos 
			  << ", result:  '" << result << "'" 
			  << std::endl;
			  
	// Todo trim to the right 
	pos = result.find_last_not_of (" \t");
	
	if (pos != std::string::npos) 
	{
		result = result.substr (0, pos + 1);
	}
	
	std::cout << "right -- pos: " << pos 
			  << ", result:  '" << result << "'" 
			  << std::endl;
	
	// Normal function termination
	return result;
}

int 
mySmallRandom () 
{
	int result;

	result = rand () % 32768;
	
	// Normal function termination 
	return result;
}

int 
unaryMinusOrNot ()
{
	int result = (mySmallRandom () > UNARY_MINUS)? +1: -1;
	
	// Normal function termination
	return result;
}

/**
 * \brief splits a string into a vector of strings, à la Python's split()
 * @param text The string to be split
 * @param separator The delimiter to be used in the splitting
 * @return The vector of strings from the splitting of the text string
 */ 
 
std::vector<std::string> 
split (const std::string& text, const std::string separator)
{
	std::string               aux;
	std::string::size_type    pos;
	std::vector<std::string> result;
	
	aux = text + separator;
	pos = aux.find (separator);
	if (pos == std::string::npos) 
	{
		result.push_back (std::string (text));
		return result;
	}
	
	size_t ind = 0;
	while ((pos != std::string::npos) && (aux.length () > 0))
	{
		if (pos != 0)
		{
			result.push_back (aux.substr (0, pos));
			std::cout << "aux.substr (0, " << pos -1 << ") == '"
					  << aux.substr (0, pos) << "'"
					  << std::endl;
		}
		else 
		{
			result.push_back (std::string (""));
		}
		
		if (pos < (aux.length () - 1))
		{
			aux = aux.substr (pos + 1);
		}
		else 
		{
			aux.resize (0);
		}
		std::cout << "aux = '" << aux 
		          << "' result[" << ind << "] == '" 
		          << result [ind] << "' pos == " 
		          << pos
		          << std::endl;
		          
		ind++; 
		pos = aux.find (separator);
	}
	
	// Normal function termination
	return result;
}

/**
 * @class BinOp
 */
class BinOp {
protected:
	Operation op;
	int left, right;
public:
	            BinOp (Operation op, int left, int right); //< conventional constructor
	            BinOp (const std::string& expression);     //< constructor from a string 
	std::string infix () const;                            //< used as Python's  __str__()
	std::string prefix () const;                           //< used as Python's __repr__()
	std::string postfix () const;                          //< Reverse Polish Notation
	/** @todo add cast operations to string and from string */
	bool operator== ( const BinOp& that) const;            //< Equality operator 
	bool operator!= ( const BinOp& that) const;            //< Equality operator 
};

/** \brief used as Python's  __str__() 
  * 
  * @return The operation string as the usual infix <Left> <Operation> <Right>
  */
  
std::string 
BinOp::infix () 
const
{
	std::string result;
	
	result = std::to_string (this->left)  + " "
	       + operationToString (this->op) + " "
	       + std::to_string (this->right); 
	
	// Normal function termination
	return result;
}

/** used as Python's __repr__()
  * 
  * The prefix notation is also called as Polish notation. 
  * This representation can be used to construct a BinOp instance. 
  * Therefore it is similar to Python's __repr__()
  *  
  * @return The operation string as the prefix <Operation> <Left> <Right>
 */
 
std::string 
BinOp::prefix () 
const
{
	std::string result;
	
	result = operationToString (this->op) + " "	
	       + std::to_string (this->left)  + " " 
	       + std::to_string (this->right); 
	
	// Normal function termination
	return result;
}

/** Reverse Polish Notation
  * 
  * The postfix notation is also called as Reverse Polish notation. 
  *  
  * @return The operation string as the prefix <Left> <Right> <Operation>
 */

std::string 
BinOp::postfix () 
const
{
	std::string result;
	
	result = std::to_string (this->left)  + " "
	       + std::to_string (this->right) + " "
	       + operationToString (this->op); 
	
	// Normal function termination
	return result;
}

/**
  * Usual onstrutor of the class
  * 
  * @param op The binary operation to be represented
  * @param left the left operand
  * @param right the right operand 
  */  
BinOp::BinOp (Operation op, int left, int right)
{
	/** @todo parameter checking */
	this->op    = op;
	this->left  = left;
	this->right = right;
}

/**
  * constructor from a string
  * 
  * @param expression 
  */  
  
BinOp::BinOp (const std::string& expression)
{
	/** @todo parameter checking */
	std::string aux;
	std::vector <std::string> fields;
	
	aux = trim (expression);
	std::cout << "'" << aux << "'" << std::endl;

	fields = split (aux, " ");
	for (size_t ind = 0; ind < fields.size (); ind++)
	{
		std::cout << "fields[" << ind << "] == '" << fields[ind] 
		          << "'" << std::endl;
	}
	
	this->op    = stringToOperation (fields[0]);
	this->left  = std::stoi (fields[1]);
	this->right = std::stoi (fields[2]);
}

//< Equality operator 
bool 
BinOp::operator== ( const BinOp& that) const
{
	bool result = true;
	
	if ((this->op    != that.op)    ||
	    (this->left  != that.left)  ||
	    (this->right != that.right) 
	    )
	{
		result = false;
	} 
	
	return result;
}

//< Inequality operator 
bool 
BinOp::operator!= ( const BinOp& that) const
{	
	return ! operator == (that);
}

int 
main ()
{
	int       iExpr;     //< index of expressions

#if !defined(TEST)
	int       n;
	int       iOperands; //< index of operands
	int       nOperands; //< number of operands
#endif /* !defined(TEST) */

	int       left;      //< left operand
	int       right;     //< right operand	
	Operation op;        //< index of operator
/*
	char     buf[16 * 1024] = {0}; //< to zero buffer
	char     partBuf[1024] = {0}; //< to zero buffer
 */ 
	unsigned int seed;

	std::vector <BinOp> exprSequence;

	seed = (unsigned int)time (NULL);
	srandom (seed);
	
	/** @todo loop to generate expressions and values */
	for (iExpr = 0; iExpr < TOT_EXPR; iExpr++)
	{
		op    = (Operation)(mySmallRandom () % (opLAST + 1));
		left  = mySmallRandom () * unaryMinusOrNot ();
		right = mySmallRandom () * unaryMinusOrNot ();
		exprSequence.push_back (BinOp (op, left, right));
	}
		
	/** @todo dump expression sequence as a string to cout */
	for (iExpr = 0; iExpr < TOT_EXPR; iExpr++) 
	{
		BinOp* bo = new BinOp (exprSequence[iExpr].prefix ());
		std::cout << iExpr << ": " << exprSequence[iExpr].prefix () << std::endl;
		
		if (exprSequence[iExpr] != *bo) 
		{
			std::cout << "\t Different from " << bo->prefix () << std::endl;
		}
	}
	
	// Normal function termination
	return 0;
}
