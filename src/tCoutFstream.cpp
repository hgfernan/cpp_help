/**
  * Polymorphism between std::cout and fstream
  */
  
#include <iostream>
#include <fstream>

int 
main (int argc, char** argv)
{
	std::ostream* outStream;
	
	if (argc > 1) {
#if 0
		std::ofstream ofs;
		ofs.open (argv[1]);
		outStream = &ofs;
#else 
		outStream = new std::ofstream (argv[1]);
		
#endif /* 0*/
	} else {
		outStream = &std::cout;
	}
	
	(*outStream) << "Hello, world !" << std::endl;
	
	outStream->flush ();
	
	return 0;
}
