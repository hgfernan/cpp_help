/**
  Sample code for fstream::seekg()

  Created on Fri 21 Jul 2023 12:55:27 AM -03
  @author: hilton
  */

#include <iostream>
#include <fstream> // class ifstream

int main()
{
    const std::string inp_name = "/etc/profile";
    std::fstream inp_f(inp_name,  std::ios::in | std::ios::binary);

    // OBS Go to end to get filesize
    auto n = inp_f.seekg(0, std::ios::end).tellg();
    std::cout << "Filesize of '" << inp_name << "' " << n << std::endl;

    // OBS the result of seekg() can't be saved in a variable, for that would call the deleted fstream's default constructor

    // OBS Rewind
    n = inp_f.seekg(0).tellg();

    std::cout << "Position at the beginning " << n << std::endl;

    return 0;
}
