/**
 * Slightly adapted from from:
 * <ul>
 * 	<li>
 * 		<a href="http://www.cplusplus.com/reference/string/basic_string/find/">
 * 			std::basic_string::find -- C++ reference
 * 		</a>
 * 	</li>
 * </ul>
 */
// string::find
#include <iostream>
#include <string>

int main ()
{
  std::string str ("There are two needles in this haystack with needles.");
  std::string item1 ("needle");
  const char* item2 = "needles are small";
  std::string item3 ("haystack");
  
  std::cout << "The string to be searched is '" << str << "'" << "\n\n";

  // different member versions of find in the same order as above:
  std::cout 
	<< "Using 'size_type find (const basic_string& str, size_type pos = 0) const;'" 
	<< std::endl;
  std::cout << "Will search item '" << item1 << "'" << '\n';
  std::string::size_type found = str.find (item1);
  if (found != std::string::npos) 
  {
    std::cout << "first '" << item1 << "' found at: " << found << "\n\n";
  }
  else 
  {
    std::cout << "'" << item1 << "' was not found in the string." << "\n\n";
  }

  std::cout 
	<< "Using 'size_type find (const charT* s, size_type pos, size_type n) const;'" 
	<< std::endl;
  std::cout 
	<< "Will search item '" << item2 
	<< "', from position " << (found + 1) 
	<< ", starting from " << 6
	<< std::endl;
  found = str.find (item1.c_str (), found + 1, 6);
  if (found != std::string::npos)
  {
    std::cout << "second '" << item1 <<"' found at: " << found << '\n';
  }
  else 
  {
    std::cout 
		<< "'" 
		<< item1 
		<< "' was not found in the string, in the given position and size." 
		<< '\n';
  }

  found = str.find (item3);
  if (found!=std::string::npos) 
  {
    std::cout << "'" << item3 << "' also found at: " << found << "\n\n";
  }
  else 
  {
    std::cout << "'" << item3 << "' was not found.\n\n";
  }

  found=str.find ('.');
  if (found!=std::string::npos)
    std::cout << "Period found at: " << found << '\n';

  // let's replace the first needle:
  str.replace (str.find(item1), item1.length(),"preposition");
  std::cout << str << '\n';

  return 0;
}
