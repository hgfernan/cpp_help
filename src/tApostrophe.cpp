#include <cstdio>
#include <clocale>

int 
main(void) {
  int           iBig = 123456789;
  long          lBig = 123456789L;
  unsigned long ulBig = (unsigned long)iBig;
  float         fBig = (float)iBig + 0.123f;
  double        dBig = (double)iBig + 0.123;

  setlocale(LC_ALL, ""); 

  printf ("int          %'d\n",  iBig);
  printf ("long         %'ld\n", lBig);
  printf("unsigned long %'lu\n", ulBig);
  printf("float         %'f\n",   fBig);
  printf("double        %'f\n",   dBig);

  /* Normal function termination */
  return 0;
}
