/**
    Histogram and similar counting for a binary file
    Thu 20 Jul 2023 06:45:41 PM -03
    @author: hilton
 */
#include <iostream>
#include <fstream> // class ifstream
#include <iomanip> // class ifstream

int main(int argc, char* const* argv)
{
    int c = 0;
    int prev_c = -1;
    int freq[256] = {0};
    int mark[256][256] = {{0}};

    if (argc < 2) {
        std::cerr << argv[0] << " ERROR Input file was not informed" << std::endl;

        // Return to indicate failure
        return 1;
    }

    std::ifstream inp_f(argv[1], std::ios::binary);
    if (inp_f.fail()) {
        std::cerr << argv[0] << "ERRR Could not open file '" << argv[1] << "' for input" << std::endl;

        // Return to indicate failure
        return 2;
    }

    for (c = inp_f.get(); ! inp_f.eof(); c = inp_f.get()) {
        if (prev_c != -1) {
            mark[prev_c][c]++;
        }
        freq[c]++;
        prev_c = c;
    }

    for (c = 0; c < 256; c++) {
        std::cout << "0x"
                  << std::resetiosflags(std::ios_base::dec)
                  << std::setiosflags(std::ios_base::hex)
                  << std::setprecision(2)
                  << std::setw(2)
                  << std::setfill('0')
                  << c << " "
                  << std::resetiosflags(  std::ios_base::hex
                                      | std::ios_base::showbase)
                  << std::setiosflags(std::ios_base::dec)
                  << std::setfill(' ')
                  << std::setw(6)
                  << freq[c]
                  << std::endl
        ;
    }

    for (prev_c = 0; prev_c < 256; prev_c++) {
        std::cout << "0x"
                  << std::resetiosflags(std::ios_base::dec)
                  << std::setiosflags(std::ios_base::hex)
                  << std::setprecision(2)
                  << std::setw(2)
                  << std::setfill('0')
                  << prev_c
                  << std::endl
        ;

        for (c = 0; c < 256; c++) {
            std::cout << "\t0x"
                      << std::resetiosflags(std::ios_base::dec)
                      << std::setiosflags(std::ios_base::hex)
                      << std::setfill('0')
                      << std::setprecision(2)
                      << std::setw(2)
                      << c << " "
                      << std::setiosflags(std::ios_base::dec)
                      << std::setfill(' ')
                      << std::setw(6)
                      << mark[prev_c][c]
                      << std::endl
            ;
        }
    }

    // Normal function termination
    return 0;
}
