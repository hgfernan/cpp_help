/**
 * \file times.cpp
 * 
 * \brief An emulation of sys/times.h under Win32 
 * 
 * A library designed to emulate sys/times.h under Win32. A special 
 * version of the system() call is also needed to get times() 
 * functionality for  * a child started by system().
 * \author hgfernan
 * \date 2018-02-18
 * \note adapted from Debian 4.9.65-3+deb9u2 (2018-01-04) x86_64 GNU/Linux
 */

#include <times.h> ///< my emulation of sys/times.h


#include <windows.h>
#include <stdio.h>

static int    _lastError          = 0;
static HANDLE _childThreadHandle  = (HANDLE)NULL;
static HANDLE _childProcessHandle = (HANDLE)NULL;

int
myGetLastError (void) 
{
	return _lastError;
}

int
mySetLastError (int lastError) 
{
	_lastError = lastError;
	return _lastError;
}

LPSYSTEMTIME
FileTimeToLocalTime (LPSYSTEMTIME lpstResult, LPFILETIME lpftInput)
{
	BOOL dwRv;
	FILETIME stUTC;
	
	dwRv = FileTimeToSystemTime (lpftInput, &stUTC);
	if (! dwRv) 
	{
		mySetLastError (GetLastError ());
		
		return (SYSTEMTIME*)NULL;
	}
	
    dw = SystemTimeToTzSpecificLocalTime(NULL, &stUTC, lpstLocal);
	if (! dwRv) 
	{
		mySetLastError (GetLastError ());
		
		return (SYSTEMTIME*)NULL;
	}

	result lpstResult;
} 

clock_t
SystemTimeToSeconds (LPSYSTEMTIME lpstInput) 
{
	clock_t result = 0;
	
	result  = lpstInput->wHour;
	result *= 60;
	result += lpstInput->wMinute;
	result *= 60;
	result += lpstInput->wSecond;
	result *= 1000;
	result += lpstInput->wMilliseconds;
	result *= CLOCKS_PER_SEC / 1000;
	
	// Normal function termination
	return result;
}

static int 
_mySetHandles (HANDLE hProcess, HANDLE hThread) 
{
	_childThreadHandle  = hThread;
	_childProcessHandle = hProcess;
	
	return 0;
}

static HANDLE 
_myGetChildThreadHandle ()
{
	return _childThreadHandle;
}

static HANDLE 
_myGetChildProcessHandle ()
{
	return _childProcessHandle;
}

int 
mySystem (const char* command) 
{
	BOOL bRv;
	DWORD dwRv;
	STARTUPINFO si;
    PROCESS_INFORMATION pi;

    ZeroMemory (&si, sizeof (si));
    si.cb = sizeof (si);
    ZeroMemory (&pi, sizeof (pi));

    // Start the child process. 
    if( !CreateProcess(NULL,    //!< No module name (use command line)
                       command, //!< Command line
					   NULL,    //!< Process handle not inheritable
					   NULL,    //!< Thread handle not inheritable
					   FALSE,   //!< Set handle inheritance to FALSE
					   0,       //!< No creation flags
					   NULL,    //!< Use parent's environment block
					   NULL,    //!< Use parent's starting directory 
					   &si,     //!< Pointer to STARTUPINFO structure
					   &pi )    //!< Pointer to PROCESS_INFORMATION structure
    ) 
    {
//        printf( "CreateProcess failed (%d).\n", GetLastError() );
		mySetLastError (GetLastError ());
		
        return myGetLastError ();
    }

    // Wait until child process exits.
    dwRv = WaitForSingleObject (pi.hProcess, INFINITE);
    if (WAIT_OBJECT_0 != dwRv) 
    {
		mySetLastError (GetLastError ());
		
        return myGetLastError ();		
	}

/**
 * \note Will process child times be saved, even after its process is
 *       closed.
 */
 
	// Close process and thread handles.
	bRv = CloseHandle (pi.hProcess);
	if (! bRv)
	{
		mySetLastError (GetLastError ());
		
        return myGetLastError ();		
	}
	
	bRv = CloseHandle (pi.hThread);
	if (! bRv)
	{
		mySetLastError (GetLastError ());
		
        return myGetLastError ();		
	}
 
	// Save the handles of the child.
	_mySetHandles (pi.hProcess, pi.hThread);
	
	return ERROR_SUCCESS;
}


/**
 * Store the CPU time used by this process and all its dead children 
 * (and their dead children) in \p __buffer.
 * @param[out] __buffer A struct tms that will hold real, system and user 
 *        time of this process and all of its children.
 * @return The number of clock ticks since an arbitrary point in the 
 * 	       past, possibly the previous execution of times(), or 
 *         (clock_t)-1 for errors. 
 */
 
clock_t 
times (struct tms *__buffer)
{
	BOOL rv;
	clock_t result = 0;
	
	FILETIME processCreationTime = {0};
	FILETIME processExitTime = {0};
	FILETIME processKernelTime = {0};
	FILETIME processUserTime = {0};
	FILETIME childCreationTime = {0};
	FILETIME childExitTime = {0};
	FILETIME childKernelTime = {0};
	FILETIME childUserTime = {0};
	
	/* Get child times */
	rv = GetProcessTimes (_myGetChildProcessHandle (), 
						  &childCreationTime, 
						  &childExitTime, 
						  &childKernelTime, 
						  &childUserTime
						 );
						 
	if (! rv) 
	{
		mySetLastError (GetLastError ());
		
        return myGetLastError ();		
	}
	
	/* Get process times */
	rv = GetProcessTimes (GetCurrentProcess (),
						  &processCreationTime, 
						  &processExitTime, 
						  &processKernelTime, 
						  &processUerTime
						 );
	if (! rv) 
	{
		mySetLastError (GetLastError ());
		
        return myGetLastError ();		
	}

	// TODO calculate times
	
	
	
	// Normal function termination
	return result;
}
