/**
  * @file tRegex2.cpp
  * @since 2018-04-30
  * @note A tiny sample of <regex> resources 
  * 
  * Slightly changed from
  * <ol>
  * 	<li>
  * 		<a href="http://www.cplusplus.com/reference/regex/regex_match/">
  * 			std::regex_match
  * 		</a>
  * 	</li>
  * </ol>
  */
  
// regex_match example
#include <iostream>
#include <string>
#include <regex>

#define TEST_STR "subject"
#define TEST_RE  "(sub)(.*)"

#define NELEM(arr, baseType) (sizeof (arr) / sizeof (baseType))

int main ()
{
  const char cstr[] = TEST_STR;
  std::string s (TEST_STR);
  std::regex e (TEST_RE);

  std::cout << "All examples below use: "
            << "TEST STRING '" << TEST_STR << "'" << " and " 
            << "REGEX '" << TEST_RE << "'" << std::endl;
            
  std::cout << "\nUsing std::regex_match() with strings" << std::endl;
  if ( std::regex_match (TEST_STR, std::regex (TEST_RE)) ) {
    std::cout << "string literal matched\n";
  }

  std::cout << "\nUsing std::regex_match() with string and precompiled regex" 
            << std::endl;
  if (std::regex_match (s, e)) {
    std::cout << "string object matched\n";
  }
            
  std::cout << "\nUsing std::regex_match() with iterators and precompiled regex" 
            << std::endl;
  if (std::regex_match (s.begin (), s.end (), e)) {
    std::cout << "range matched\n";
  }

  std::cout << "\nUsing struct cmatch for range information -- 'c' for char" 
            << std::endl;
  std::cmatch cm;    // same as std::match_results<const char*> cm;
  std::regex_match (cstr, cm, e);
  std::cout << "string literal with " << cm.size() << " matches\n";

  std::cout << "\nUsing struct smatch for range information -- 'c' for string iterator" 
            << std::endl;
  std::smatch sm;    // same as std::match_results<string::const_iterator> sm;
  std::regex_match (s, sm, e);
  std::cout << "string object with " << sm.size() << " matches\n";

  std::cout << "\nUsing struct smatch for range information with iterators" 
            << std::endl;
  std::regex_match (s.cbegin (), s.cend (), sm, e);
  std::cout << "range with " << sm.size() << " matches\n";

  // using explicit flags: match_default doesn't change the regex behavior
  std::cout << "\nUsing regex_match() with explicit flags -- can change regex behavior" 
            << std::endl;
  std::regex_match (cstr, cm, e, std::regex_constants::match_default);

  std::cout << "\nthe matches were: ";
  for (unsigned i = 0; i < cm.size (); ++i) {
    std::cout << "[" << cm[i] << "] ";
  }
		
  std::cout << std::endl;

  std::regex intRE   ("[+-]{0,1}[0-9]+");
  std::regex floatRE ("[+-]{0,1}[0-9]+(\\.[0-9]*){0,1}");
  std::regex sciRE   ("[+-]{0,1}[0-9]+(\\.[0-9]*){0,1}([Ee@][+-]{0,1}[0-9]{1,3})");
  
  const char* numbers[] = { "123456"   , "+123456"     , "-123456", 
                           "123.456"   , "+123.456"    , "-123.456",
                           "123.456e78", "+123.456E+78", "-123.456@-78",
                           "ABC"       , "Abc"         , "abc", 
                           "123ABC"    , "ABC123"      , "1a2b3c"
                          };
  std::cout << "\nsizeof (numbers) == " << sizeof (numbers) << std::endl;
  std::cout << "sizeof (numbers) / sizeof (char*) == " << (sizeof (numbers) / sizeof (char*)) << std::endl;
  std::cout << "NELEM (numbers, char*) == " << NELEM (numbers, char*) << std::endl;
  
  std::cout << std::endl; 
  
  std::vector <std::string> vNumbers (numbers, numbers + NELEM (numbers, char*));
  
  bool someMatch;
  for (std::string &n : vNumbers)
  {
    someMatch = false;
    std::cout << n << std::endl;
    if (std::regex_match (n, intRE))
    {
      std::cout << "\tis INTEGER" << std::endl;
      someMatch = true;
    } 
    
    if (std::regex_match (n, floatRE))
    {
      std::cout << "\tis FLOATING POINT" << std::endl;
      someMatch = true;
    }
    if (std::regex_match (n, sciRE))
    {
      std::cout << "\tfollows SCIENTIFIC notation" << std::endl;
      someMatch = true;
    }
    
    if (! someMatch) {
      std::cout << "\tis NEITHER integer nor FLOATING POINT, nor SCIENTIFIC" << std::endl;
    }
  }

  return 0;
}
