/**
  * @file myRegexLab.cpp
  * @note A tiny experience of <regex> resources for changing structs
  * <p>
  * Even though the replace API looks more indicated in this case,
  * it seemed a little difficult to handle.
  * </p>
  * @since 2018-04-30
  * 
  * Slightly changed from
  * <ol>
  *     <li>
  *         <a href="http://www.cplusplus.com/reference/regex/match_results/position/">
  *             std::match_results::position
  *         </a>
  *     </li>
  * </ol>
  */

#include <iostream>
#include <string>
#include <regex>

int main ()
{
  std::string s       ("    1234 145 324 abbcd");
  std::string reconstr;
  std::string indices ("0123456789a123456789b1");
  std::smatch m;
  const char mask[] = "[0-9]+";
  std::regex e (mask);   // matches words beginning by "sub"

  std::cout << "Target sequence: '" << s << "'" << std::endl;
  std::cout << "                  " << indices  << std::endl;
  std::cout << "Regular expression: '" << mask << "'" << std::endl;
  std::cout << "The following matches and submatches were found:" 
            << std::endl;

  size_t pos = 0;
  while ( std::regex_search (s, m, e))
  {
    if (m.size () > 1) 
    {
      std::cout << "number of matches (" << m.size () 
                << ") larger than 1" << std::endl;
    }
    for (std::size_t ind = 0; ind < m.size (); ind++)
    {
        std::cout << "match " << ind
                  << " at position " << m.position (ind)
                  << " is '" << m[ind] << "'" << std::endl;
        pos += m.position (ind) + m[ind].length ();
    }
    std::cout << "We're now at " << pos << std::endl;
    std::cout << "The suffix is '" << m.suffix ().str () 
              << "'" << std::endl;
    std::cout << "The prefix is '" << m.prefix ().str () 
              << "'" << std::endl;
    
    s = m.suffix ().str ();
  }

  return 0;
}
