/**
  * \todo create the file header
  *
  */

#include <cstdlib> // exit()
#include <cstring> // strlen()

#include "YaCsvParser.hpp"
#include "TyApp.hpp"

int 
main (int argc, char** argv)
{
	TyApp* app;
	YaCsvParser* parser;
	std::size_t nRecords;
	std::string inpNam;
	std::string strBuf;
	std::vector <std::string> fields;
	
	const std::string minTestNam ("min_test.csv");
	
	/** \todo handle command line parsing exception */
	app = new TyApp (argc, argv);
	std::cerr << "app: " << app << std::endl;
	std:cerr.flush ();
	
	TyAPP::setGlobal (app);

	app->basWrite ("Will parse command line\n");
	
	if (! app->parseCmdLine ())
	{
		/** \todo implement app name in TyApp */
		/** \todo implement error msging in TyApp */
		strBuf = "*** " + std::string (argv[0])
			   + ": error in parsing command line\n";
		app->basWrite (strBuf);
				 
		// Return to indicate failure 
		return 1;
	}

	app->basWrite ("Will interpret params\n");
	
	if (! app->interpretParams ())
	{
		/** \todo implement app name in App */
		/** \todo implement error msging in App */
		strBuf = "*** " + std::string (argv[0]) 
			   + ": error in parsing command line\n";
		app->basWrite (strBuf);
				 
		// Return to indicate failure 
		return 2;
	}
	 
	parser = new YaCsvParser (app->getFieldSep (), app->getQuote ());

	
	app->outWrite ("*** YaCsvParser: Yet another CSV parser ***\n");
	strBuf = "Field separator: '"  
		   + escapeString (parser->getFieldSep ())
		   + "'\n";
	app->outWrite (strBuf);
			  
	strBuf = "Line separator: '" 
		   + escapeString (parser->getLineSep ())
		   + "'\n";
	app->outWrite (strBuf);
	
	strBuf = "Quote string: '"
	       + escapeString (parser->getQuote ())
	       + "'\n";

	// If file name was not provided generate its own data
	inpNam = app->getInpFileName (); 
	if (inpNam == "") 
	{
		const std::string minTestLine ("\"a\"; 10; a\n");
		
		std::ofstream minTestStream (minTestNam);
		if (! minTestStream.is_open ()) 
		{
			strBuf = "*** Error: could not open file '" 
			       + minTestNam
			       + "' for output ***\n";
			app->basWrite (strBuf);
		
			// Return to indicate failure
			return 1;			
		}

		minTestStream << minTestLine << std::endl;
		minTestStream.close ();
		
		inpNam = minTestNam;
    } 
    strBuf = "*** Using file '" + inpNam + "' for input ***\n"; 
    app->outWrite (strBuf);
	
	if (! parser->setInputName (inpNam)) 
	{
		strBuf = "*** Error: could not open file '" 
		       + inpNam 
		       + "' for input ***\n";
		app->basWrite (strBuf);
	
		// Return to indicate failure
		return 1;			
	}

	nRecords = 0;
	while (parser->goNextLine ()) 
	{				
		nRecords++;
		fields = parser->getFields ();
		
		strBuf = "Line + " + std::to_string (nRecords) 
		       + " with " + std::to_string (fields.size ())
		       + " fields\n";
		app->outWrite (strBuf);
		
		strBuf = "Fields: " + strVectorToString (fields)  + "\n";
		app->outWrite (strBuf);
		
//
// TODO to convert to another format 
//		
//		strBuf = "";
//		for (string s : fields) 
//		{
//			if (isDecimal (s))
//			{
//				strBuf += s + ", ";
//			}
//			else 
//			{
//				strBuf += "'" + s + "', ";
//			}
//			strBuf = strBuf.substr (0, strBuf.length () - 2);
//		}
		strBuf = std::to_string (nRecords) + ", " 
		       + std::to_string (fields.size ()) + "\n";
		app->repWrite (strBuf);
	}

	strBuf = "\nRead " + std::to_string (nRecords)
	       + " record" + (( nRecords != 1)? "s": "")
	       + "\n";
	app->outWrite (strBuf);
	
	if (nRecords == 0) 
	{
		strBuf = "*** Empty file '" + inpNam + "' ***\n";
		app->basWrite (strBuf);
			
		// Return to indicate failure
		return 1;			
	}
			  
	return 0;
}
