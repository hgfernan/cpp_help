#include <iostream>

int 
main () 
{
	int          bigInt;
	int          calcBigInt;
	int	     auxInt;
	int          nIter;
	int          growing;
	unsigned int bigUInt;
//	unsigned int calcBigUInt;


	bigUInt = ~0;
	bigInt  = bigUInt >> 1;

	std::cout << "bigInt:  " << bigInt  << std::endl;
	std::cout << "bigUInt: " << bigUInt << std::endl;

	calcBigInt = 1;
	auxInt = 1;	
	
	nIter = 0;
	while (auxInt > 0) 
	{
		calcBigInt = auxInt;
		auxInt = (auxInt * 2) + 1;
		nIter++; 
	}
	
	std::cout << "nIter: " << nIter << " calc Big Int: " << calcBigInt << std::endl;
	
	growing = 1;
	for (nIter = 0; nIter < 31; nIter++) 
	{
		auxInt = calcBigInt + (nIter + 1);
		std::cout << "calc Big Int + " << (nIter + 1) << ": " << auxInt
		          << " difference: " << (calcBigInt - auxInt)
			  << " growing: " << growing 
			  << " growing difference: "  << (calcBigInt - growing)
			  << std::endl;
		growing = (2 * growing) + 1;
	}

	std::cout << std::endl;
	auxInt = 1;
	nIter  = 0;
	while ((auxInt + auxInt) == (2 * auxInt))
	{
		auxInt = auxInt + auxInt;
		nIter++;

		std::cout << nIter 
			  << " n: " << auxInt
		          << " (n + n): " << (auxInt + auxInt) 
			  << " 2*n: " << (2 * auxInt)
			  << std::endl;

		if (nIter > 31) 
		{
			break;
		}
	}

	std::cout << std::endl;
	std::cout << "iterations: " << nIter 
	          << " n: " << auxInt
                  << " (n + n): " << (auxInt + auxInt)
		  << " 2*n: "  << (2 * auxInt)
		  << std::endl;

	return 0;
}
