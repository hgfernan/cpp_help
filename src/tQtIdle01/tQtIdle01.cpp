/**
  Sample code to interrupt core application loop

  Created on Fri 28 Jul 2023 07:05:34 PM -03

  @author: hilton
  From:
  *

 */

#include <QCoreApplication>
#include <QTimer>

#include <iostream>

int main(int argc, char *argv[])
{
    QCoreApplication app(argc, argv);

    // OBS If the next line is commented, loop won't stop
    QTimer::singleShot(0, &app, QCoreApplication::quit);

    // OBS Run in the same thread as app.exec()
    std::cout << "Started counting:";
    for (int count = 0; count < 10; count++) {
        std::cout << " " << (9 - count);
    }
    std::cout << std::endl;

    return app.exec();
}
