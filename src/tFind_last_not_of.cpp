/**
  * Slightly changed from 
  * <ol>
  * 	<li>
  * 		<a href="http://www.cplusplus.com/reference/string/string/find_last_not_of/">
  * 			std::string::find_last_not_of
  * 		</a>
  * 	</li>
  * </ol>
  *
  */
   
// string::find_last_not_of
#include <iostream>       // std::cout
#include <string>         // std::string
#include <cstddef>        // std::size_t


std::string 
charToEscape (const char c) 
{
	switch (c) 
	{
	case '\0':
		return "\\0";
	case '\a':
		return "\\a";
	case '\b':
		return "\\b";
	case '\f':
		return "\\f";
	case '\n':
		return "\\n";
	case '\r':
		return "\\r";
	case '\t':
		return "\\t";
	case '\v':
		return "\\v";
	case '\\':
		return "\\\\";
	case '"':
		return "\\\"";
	case '\'':
		return "\\'";
	}

	/** \todo include all non printable characters */
	/** \todo what about UNICODE */
	
	return std::string (sizeof (char), c);
}

std::string 
escapeString (const std::string& str)
{
	std::size_t ind;
	std::string result;
	
	for (ind = 0; ind < str.length (); ind++)
	{
		result += charToEscape (str[ind]);
	}
	
	return result;
}


int main ()
{
  std::string str ("Please, erase trailing white-spaces   \n");
//                  0123456789a123456789b123456789c123456789d
  std::string whitespaces (" \t\f\v\n\r");

  std::size_t found = str.find_last_not_of (whitespaces);

  std::cout << "Whitespaces '" << escapeString (whitespaces) << "'\n";
  std::cout << "                  0123456789a123456789b123456789c123456789d"
			<< std::endl;
  std::cout << "Original string: '" << escapeString (str) << "'\n";
  std::cout << "Found a non white at " << found << std::endl;
  
  if (found != std::string::npos)
    str.erase (found + 1);
  else
    str.clear();            // str is all whitespace

  std::cout << '[' << str << "]\n";

  return 0;
}
