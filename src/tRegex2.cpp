/**
  * @file tRegex2.cpp
  * @note A tiny sample of <regex> resources 
  * @since 2018-04-30
  * 
  * Slightly changed from
  * <ol>
  * 	<li>
  * 		<a href="http://www.cplusplus.com/reference/regex/match_results/position/">
  * 			std::match_results::position
  * 		</a>
  * 	</li>
  * </ol>
  */

#include <iostream>
#include <string>
#include <regex>

int main ()
{
  std::string s ("    1234 145 324 abbcd");
  std::smatch m;
  const char mask[] = "[0-9]+";
  std::regex e (mask);   // matches words beginning by "sub"

  std::cout << "Target sequence: '" << s << "'" << std::endl;
  std::cout << "Regular expression: '" << mask << "'" << std::endl;
  std::cout << "The following matches and submatches were found:" << std::endl;

  std::regex_search (s, m, e);
  for (std::size_t ind = 0; ind < m.size (); ind++)
  {
	std::cout << "match " << ind
	          << " at position " << m.position (ind)
	          << " is '" << m[ind] << "'" << std::endl;
  }

  return 0;
}
