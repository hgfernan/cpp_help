/**
 * \file times.h
 * 
 * \brief An emulation of sys/times.h under Win32 
 * 
 * A library designed to emulate sys/times.h under Win32. A special 
 * version of the system() call is also needed to get times() 
 * functionality for  * a child started by system().
 * \author hgfernan
 * \date 2018-02-18
 * \note adapted from Debian 4.9.65-3+deb9u2 (2018-01-04) x86_64 GNU/Linux
 * 
 */

/* Copyright (C) 1991-2016 Free Software Foundation, Inc.
   This file is is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.

   The GNU C Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the GNU C Library; if not, see
   <http://www.gnu.org/licenses/>.  */

/*
 *	POSIX Standard: 4.5.2 Process Times	<sys/times.h>
 */

#ifndef	_SYS_TIMES_H
#define	_SYS_TIMES_H	1

#include <time.h>

/** \class Structure describing CPU time used by a process and its 
 *         children.  
 */
struct tms
  {
    clock_t tms_utime;		/**< User CPU time.  */
    clock_t tms_stime;		/**< System CPU time.  */

    clock_t tms_cutime;		/**< User CPU time of dead children.  */
    clock_t tms_cstime;		/**< System CPU time of dead children.  */
  };


/* Store the CPU time used by this process and all its
   dead children (and their dead children) in BUFFER.
   Return the elapsed real time, or (clock_t) -1 for errors.
   All times are in CLK_TCKths of a second.  */
// extern clock_t times (struct tms *__buffer) __THROW;

/**
 *
 */
 
extern clock_t times (struct tms *__buffer);
extern int mySystem (char* const* command);



#endif /* sys/times.h	*/

