/**
  Created on Tue 25 Jul 2023 09:37:10 PM -03
  @author: K Hong, Ph.D.
  From:
  * https://www.bogotobogo.com/Qt/Qt5_Downloading_Files_QNetworkAccessManager_QNetworkRequest.php
  */

#include "downloader.h"

#include <QDir>          // QString QDir::currentPath()
#include <QString>       // Qt strange string class
#include <QJsonDocument> // All JSON elements

Downloader::Downloader(QCoreApplication* _app)
{
    manager = new QNetworkAccessManager(this);
    netReply = nullptr;
    app = _app;
}

void Downloader::doDownload()
{
    auto url = QUrl("https://data-api.binance.vision/api/v3/exchangeInfo");
    netReply = manager->get(QNetworkRequest(url));
    connect(netReply,&QNetworkReply::readyRead,this,&Downloader::readData);
    connect(netReply,&QNetworkReply::finished,this,&Downloader::finishReading);

}

void Downloader::readData()
{
    dataBuffer.append(netReply->readAll());
}

void Downloader::finishReading()
{
    if(netReply->error())
    {
        qDebug() << "ERROR!";
        qDebug() << netReply->errorString();

        // Return to indicate failure
        app->exit(1);
    } else {
        QString line;
        line = "ContentTypeHeader: " + netReply->header(QNetworkRequest::ContentTypeHeader).toString();
        qDebug() << line;

        line = "LastModifiedHeader: '" + netReply->header(QNetworkRequest::LastModifiedHeader).toDateTime().toString() + "'";
        qDebug() << line;

        auto no = netReply->header(QNetworkRequest::ContentLengthHeader).toULongLong();
        line = "ContentLengthHeader: " + QString::number(no);
        qDebug() << line;

        no = netReply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
        line = "HttpStatusCodeAttribute: " + QString::number(no);
        qDebug() << line;

        line = "HttpReasonPhraseAttribute: " + netReply->attribute(QNetworkRequest::HttpReasonPhraseAttribute).toString();
        qDebug() << line;

        qDebug() << "";

        QString out_nam = QDir::currentPath() + "/downloaded.txt";
        QFile *file = new QFile(out_nam);
        if(file->open(QFile::Append))
        {
            QJsonDocument doc = QJsonDocument::fromJson(dataBuffer);
            QString strJson(doc.toJson(QJsonDocument::Indented));

            file->write(strJson.toStdString().c_str());
            file->write("\n");
            file->flush();
            file->close();
        }
    }

    netReply->disconnect();
    netReply->deleteLater();

    // Normal function termination
    app->exit(0);
}

/*
void Downloader::replyFinished (QNetworkReply *reply)
{
    if(reply->error())
    {
        qDebug() << "ERROR!";
        qDebug() << reply->errorString();
    }
    else
    {
        qDebug() << reply->header(QNetworkRequest::ContentTypeHeader).toString();
        qDebug() << reply->header(QNetworkRequest::LastModifiedHeader).toDateTime().toString();
        qDebug() << reply->header(QNetworkRequest::ContentLengthHeader).toULongLong();
        qDebug() << reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
        qDebug() << reply->attribute(QNetworkRequest::HttpReasonPhraseAttribute).toString();
        qDebug() << "";

//        QFile *file = new QFile("C:/Qt/Dummy/downloaded.txt");
        QFile *file = new QFile("/home/hilton/downloaded.txt");
        if(file->open(QFile::Append))
        {
            QByteArray raw = reply->readAll();
            QJsonDocument doc = QJsonDocument::fromJson(raw);
            QString strJson(doc.toJson(QJsonDocument::Indented));

            file->write(strJson.toStdString().c_str());
            file->write("\n");
            file->flush();
            file->close();
        }

        reply->close();

        delete file;
    }

    reply->deleteLater();
}
*/
