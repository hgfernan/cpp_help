#include <QTimer>
#include <QCoreApplication>

#include "runner.h"     // class Runner
#include "downloader.h" // class Downloader

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    Downloader d(&a);
    d.doDownload();

    return a.exec();
}
