#ifndef DOWNLOADER_H
#define DOWNLOADER_H

#include <QObject>
#include <QCoreApplication> // the app
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QUrl>
#include <QDateTime>
#include <QFile>
#include <QDebug>

class Downloader : public QObject
{
    Q_OBJECT
public:
    explicit Downloader(QCoreApplication* _app = nullptr);

    void doDownload();
    bool disconnect();

signals:

public slots:
//    void replyFinished (QNetworkReply *reply);
   void readData();
   void finishReading();

private:
   QCoreApplication* app;
   QNetworkAccessManager *manager;
   QNetworkReply *netReply;
   QByteArray dataBuffer;
};

#endif // DOWNLOADER_H
