/**
  * Slightly adapted from 
  * <ol>
  * 	<li>
  * 		<a href="https://github.com/jarro2783/cxxopts">
  * 			jarro2783/cxxopts
  * 		</a>
  * 	</li>
  *     <li>
  * 		<a href="https://github.com/mmmcell/cxxopts_test/blob/master/argparse_git/main.cpp">
  * 			mmmcell/cxxopts_test
  * 		</a>
  * 	</li>
  *     <li>
  * 		<a href="https://github.com/jarro2783/cxxopts/wiki/Printing-help">
  * 			Printing help -- jarro2783/cxxopts 
  * 		</a>
  * 	</li>
  * </ol>
  *  
  */
  
#include <iostream>

#include <cxxopts.hpp> // Library cxxopts

int 
main (int argc, char** argv) 
{
	bool noOp = true;
	cxxopts::Options options("MyProgram", "One line description of MyProgram");
	
	options.add_options ()
		("d,debug", "Enable debugging")
		("h,help" , "Help listing")
		("f,file" , "File name", cxxopts::value<std::string>())
		("t,times", "Number of times", cxxopts::value<std::size_t>())
		;

	auto result = options.parse(argc, argv);
	
	if (result["help"].count () > 0)
	{
		std::cout << options.help () << std::endl;
		noOp = false;
		
		exit (0);
	}

	if (result.count ("d")) 
	{
		auto debug = result["d"].as<bool> ();
		noOp = false;
		
		if (debug) 
		{
			std::cout << "Debug is enabled" << std::endl;
		}
	}
	
	if (result.count ("f")) 
	{
		auto file = result["f"].as<std::string> ();
		noOp = false;
		
		if (file.length ()) 
		{
			std::cout << "File is '" << file << "'" << std::endl;
		}
	}
	
	if (result.count ("times")) 
	{
		auto times = result["times"].as<std::size_t> ();
		noOp = false;
		
		if (times) 
		{
			std::cout << "Number of times is " << times << std::endl;
		}
	}
	
	if (noOp)
	{
		std::cout << options.help () << std::endl;
		noOp = false;
		
		exit (0);
	}
	
	// Normal function termination
	return 0;
}
