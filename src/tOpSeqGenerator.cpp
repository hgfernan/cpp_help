#include <iostream> // std::cout, std::endl
#include <ctime>    // time()
#include <cstdio>   // sprintf()
/** @todo use sprintf() equivalent for <string> */
#include <cstring>  // strlen()  
#include <cstdlib>  // srandom(), random()

const int MY_RAND_MAX = 32768;

int 
mySmallRandom () 
{
	int result;

	result = (random () % 32768);
	
	// Normal function termination 
	return result;
}

char* 
unaryMinusOrNot ( int n )
{
	static char result[128];
	
	if (mySmallRandom () < (MY_RAND_MAX / 4)) 
	{
		sprintf (result, "(-%d)", n);
	} else 
	{
		sprintf (result, "%d", n);
	}
	
	return result;
}


int 
main ()
{
	int      n;
	int      iExpr;     //< index of expressions
	int      iOperator; //< index of operator
	int      iOperands; //< index of operands
	int      nOperands; //< number of operands
	char     buf[16 * 1024] = {0}; //< to zero buffer
	char     partBuf[1024] = {0}; //< to zero buffer
	unsigned int seed;
	const int TOT_EXPR    = (10 * 1000); //< Number of expressions
	const int TOT_MAX_OPS = 20;          //< Maximum number of operators per line
	const char* binOps = "+-*/";

	seed = (unsigned int)time (NULL);
	srandom (seed);

	/** @todo issue the timestamp */


	for (iExpr = 0; iExpr < TOT_EXPR; iExpr++)
	{
		nOperands = (mySmallRandom () % TOT_MAX_OPS) + 1;
		n = mySmallRandom ();
		sprintf (buf, "constSol%05d = %s", iExpr, unaryMinusOrNot (n));
		for (iOperands = 1; iOperands < nOperands; iOperands++)
		{
			n = mySmallRandom ();
			iOperator = mySmallRandom () % strlen (binOps); 
			sprintf (partBuf, " %c %s", binOps[iOperator], unaryMinusOrNot (n));
			strcat (buf, partBuf);
		}
		strcat (buf, ";");
		std::cout << buf << std::endl;
	}	
	
	// Normal function termination
	return 0;
}
