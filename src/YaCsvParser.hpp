/**
  * \todo create the file header
  *
  */
  
#if !defined(_YACSVPARSER_H)
#define _YACSVPARSER_H

#include <vector>
#include <iostream>
#include <fstream>
#include <utility> // pair

#include <cstdio>
#include <cstring> // strcpy(), strlen()

#include "TyApp.hpp" /* TyApp::setGlobal(), TyApp::getGlobal() */

const std::size_t G_BUF_LEN = (1024 * 1024);

std::string charToEscape (const char c); 
std::string escapeString (const std::string& str);
bool        isDecimal    (const std::string& s);
std::string trim         ( const std::string& input, const std::string& delim); 

/** \todo use templates for generic transformation */

std::string size_tVectorToString (std::vector <std::size_t> input); 
std::string strVectorToString (std::vector <std::string> input); 

class YaCsvParser 
{
protected:
	static const std::size_t BUF_LEN;  /**< Default buffer length */
	static const std::size_t BUF_INC; /**< Default buffer growth increment */
	
	bool parsed;             /**< A line just read was already parsed */
	char partBuf[G_BUF_LEN]; /**< Partial buffer */
	char* recBuf;            /**< Record buffer, used for */
	std::string fldSep;      /**< Field separator */
	std::string lineSep;     /**< Line separator */
	std::string quote;       /**< Quote to use to delimit strings */
	std::ifstream inpStream; /**< Input file */
	std::vector<std::string> fields; /**< A vector of fields */
	std::size_t maxLine;     /**< max line len, w/o the line seps */
	std::size_t recBufSize;  /**< current record buffer size */
	std::size_t recBufUsed;  /**< current record buffer length in use */
	
	TyApp* app;
	
public:
	static const std::string DEF_FLDSEP;   /**< Default buffer field separator */
	static const std::string DEF_LINESEP;  /**< Default buffer line separator */
	static const std::string DEF_QUOTE;    /**< Default quote character */

	YaCsvParser (); /**< Default constructor */
//  TODO 	YaCsvParser (string fldSep, string lineSep, string quote);
	YaCsvParser (std::string fldSep, std::string quote);
	YaCsvParser (std::string inpName);
//	TODO 	YaCsvParser (FILE* );
//	TODO 	YaCsvParser (stream like cout);
//	TODO 	YaCsvParser (stream like ifstream);
	~YaCsvParser ();
	bool setInputName (std::string inpName);
	bool goNextLine ();
	std::vector<std::string> getFields ();
	std::string getField (std::size_t index);
	std::size_t fieldCount ();
	std::string getFieldSep () const { return this->fldSep; };
	std::string getLineSep () const { return this->lineSep; };
	std::string getQuote () const { return this->quote; };
};

#endif /* !defined(_YACSVPARSER_H) */
