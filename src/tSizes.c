#include <stdio.h>

#define NAME_SIZEOF(x) #x, sizeof (x)
int 
main (void) {
	printf ("sizeof (%s) == %lu\n", NAME_SIZEOF (char));
	printf ("sizeof (%s) == %lu\n", NAME_SIZEOF (short));
	printf ("sizeof (%s) == %lu\n", NAME_SIZEOF (int));
	printf ("sizeof (%s) == %lu\n", NAME_SIZEOF (long));
	printf ("sizeof (%s) == %lu\n", NAME_SIZEOF (long long));
	printf ("sizeof (%s) == %lu\n", NAME_SIZEOF (size_t));

	// Normal function termination
	return 0;
}

