/**
  * istream::getline example
  * Slightly adapted from
  * <ol>
  * 	<li>
  * 		<a href="http://www.cplusplus.com/reference/istream/istream/getline/">
  * 			std::istream::getline
  * 		</a>
  * 	</li>
  * 	<li>
  * 		<a href="http://www.cplusplus.com/reference/istream/istream/gcount/">
  * 			std::istream::gcount
  * 		</a>
  * 	</li>
  * </ol> 
  * 
  */ 

// istream::getline example

#include <iostream>  // std::cin, std::cout, std::cin.gcount()
#include <cstring>   // strlen()

int main () {
  char name[256], title[256];

  std::cout << "Please, enter your name: ";
  std::cin.getline (name, 256);
  std::cout << std::cin.gcount () << " characters read.\n";
  std::cout << strlen (name) << " characters transferred.\n";

  std::cout << "Please, enter your favourite movie: ";
  std::cin.getline (title,256);
  std::cout << std::cin.gcount () << " characters read.\n";
  std::cout << strlen (title) << " characters transferred.\n";

  std::cout << name 
			<< "'s favourite movie is \"" 
			<< title 
			<< "\"."
			<< std::endl;
  
  return 0;
}
