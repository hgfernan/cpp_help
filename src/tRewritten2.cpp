#include <iostream>

struct B {};
struct A {
	    bool operator==(B const&) { std::cout << "1\n"; return true; }
};
bool operator==(B const&, A const&) { std::cout << "2\n"; return true; }

int main() {
	auto b = B{}; auto a = A{};

	b ==          a;  	   // outputs: 1
	(const B)b ==          a;  //          1
	b == (const A)a;  	   //          2
	(const B)b == (const A)a;  //          2
}

