/**
  * \todo create the file header
  *
  */

#if !defined(_TYAPP_HPP)
#define _TYAPP_HPP

#include <string>  // class string 
#include <fstream> // class ofstream 

class TyApp 
{
protected:
	int argc;
	bool parsed; 
	char** argv;
	
	bool debug; /**< Debug enabled */
	bool echo;  /**< Echo of results enabled */
	bool terse; /**< Terse reporting enabled */
	
	std::string basFileName;
	std::string inpFileName;
	std::string outFileName;
	std::string repFileName;
	std::string fieldSep;
	std::string quote;
	
	std::ofstream basStream;
	std::ofstream outStream;
	std::ofstream repStream;
	
/** \todo use the 'parsed' attribute to avoid calling functions before parsing */
	
public: 
	TyApp (const int argc, char** argv);
	bool parseCmdLine ();
	bool interpretParams ();
	std::string getFieldSep () const { return this->fieldSep; };
	std::string getQuote () const { return this->quote; };
	std::string getBasFileName () const { return this->basFileName; };
	std::string getInpFileName () const { return this->inpFileName; };
	std::string getOutFileName () const { return this->outFileName; };
	std::string getRepFileName () const { return this->repFileName; };

	/* \todo create a stream operator for the output functions */
	std::size_t basWrite (const std::string& buf);
	std::size_t outWrite (const std::string& buf);
	std::size_t repWrite (const std::string& buf);
};

extern TyApp* setGlobal (TyApp* app);
extern TyApp* getGlobal ();

/** \todo implement singleton */

#endif /* !defined(_TYAPP_HPP) */
