// From https://stackoverflow.com/questions/64130311/what-are-the-breaking-changes-caused-by-rewritten-comparison-operators
//

#include <iostream>

using namespace std;

struct B {};

struct A
{
	    bool operator==(B const&) { cout << "A::operator==" << endl; return false; }  // #1
};

bool operator==(B const&, A const&)  // #2
{
	cout << "::operator==" << endl;
	return true;
}

int main()
{
	  B{} == A{};  // c++17: calls #2
	                 // c++20: calls #1
} 
 
